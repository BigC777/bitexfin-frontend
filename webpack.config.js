// Include webpack plugin
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CreateFileWebpack = require('create-file-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
// Include libs
const webpack = require('webpack');
const path = require('path');
const md5 = require('md5');

const now = new Date();
const version = {
  hash: md5(now),
  date: now
};

const config = {
  mode: 'production',
  devtool: '',
  entry: {
    admin: [
      'babel-polyfill',
      path.resolve(__dirname, 'resources/styles/admin.scss'),
      path.resolve(__dirname, 'resources/scripts/admin.js')
    ],
    main: [
      'babel-polyfill',
      path.resolve(__dirname, 'resources/styles/main.scss'),
      path.resolve(__dirname, 'resources/scripts/main.js')
    ]
  },
  output: {
    filename: 'assets/scripts/[name].min.js',
    path: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'resources/scripts'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(sass|scss|css)$/,
        include: [path.resolve(__dirname, 'resources/styles'), path.resolve(__dirname, 'node_modules')],
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { url: false }
            },
            {
              loader: 'sass-loader',
              query: {
                outputStyle: 'compressed',
                sourceMapContents: true,
                sourceMap: true
              }
            }
          ]
        })
      }
    ]
  },
  watchOptions: {
    // aggregateTimeout: 1000,
    ignored: /node_modules/,
    poll: 3000
  },
  resolve: {
    extensions: ['.js', '.json', '.vue'],
    alias: {
      '@': path.join(__dirname, './resources/scripts'),
      jquery: 'jquery/dist/jquery.min.js',
      vue: 'vue/dist/vue.min.js'
    }
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'assets/styles/[name].min.css',
      // ignoreOrder: true
    }),
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      Vue: 'vue',
    }),
    new HtmlWebpackPlugin({
      template: 'resources/views/main.html',
      version: version.hash,
      inject: false
    }),
    new CreateFileWebpack({
      content: JSON.stringify(version),
      path: path.join(__dirname, '.'),
      fileName: 'version.json'
    })
  ]
};

if (process.env.NODE_ENV === 'development') {
  config.resolve.alias.vue = 'vue/dist/vue.js';
  config.devtool = 'source-map';
  config.mode = 'development';
}

module.exports = config;
