import InputMasked from '@aymkdn/vue-input-masked';
import ModalWrapper from "./main/components/ModalWrapper";
import InputSpinner from './common/form-elements/InputSpinner'
import DashboardLayout from './main/layouts/Dashboard'
import NewDashboardLayout from './main/layouts/NewDashboard'
import ErrorView from './common/form-elements/Errors'
import TerminalLayout from './main/layouts/Terminal'
import SuccessView from './common/form-elements/Success'
import SimpleVueValidation from 'simple-vue-validator'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import EmptyLayout from './main/layouts/Empty.vue'
import MainLayout from './main/layouts/Main.vue'
import RightPanel from './common/right-panel'
import App from './main/Application.vue'
import {maintenanceRouter, router} from './main/router'
import store from './main/stores/index'
import {sync} from 'vuex-router-sync'
import vuexI18n from 'vuex-i18n';
import translationsEn from './lang/en.json';
import translationsRu from './lang/ru.json';


import Network from './common/plugins/network'
import IdleVue from 'idle-vue'
import VueCookies from 'vue-cookies'
import VTooltip from 'v-tooltip'
import Vue from 'vue'
import {mapGetters} from 'vuex'

import Config from './common/utilities/loaders/config'
import Csrf from './common/utilities/csrf'
import requests from './main/requests'
import plugins from "./main/plugins";

import VueNativeSock from 'vue-native-websocket'

import Router from 'vue-router'

Vue.use(Router)
Vue.use(vuexI18n.plugin, store);
Vue.use(plugins)

Vue.i18n.add('EN', translationsEn);
Vue.i18n.add('RU', translationsRu);

Vue.i18n.set(localStorage.getItem('lang') || 'EN');

/**
 * Set global plugins
 */
Vue.config.productionTip = false

/**
 * Set global plugins
 */
const eventsHub = new Vue();
Vue.use(SimpleVueValidation)
Vue.use(PerfectScrollbar)
Vue.use(VueCookies)
Vue.use(VTooltip)
Vue.use(Network, {store})
Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 60000 * 15
})


/**
 * Global $socket plugin
 * https://www.npmjs.com/package/vue-native-websocket
 */

Vue.use(VueNativeSock, 'ws://localhost:8080', {
  connectManually: true,
  reconnection: true, // (Boolean) whether to reconnect automatically (false)
  //reconnectionAttempts: 5, // (Number) number of reconnection attempts before giving up (Infinity),
  reconnectionDelay: 3000, // (Number) how long to initially wait before attempting a new (1000)
})

/**
 * Set global component
 */
Vue.component('dashboard-layout', DashboardLayout);
Vue.component('new-dashboard-layout', NewDashboardLayout);
Vue.component('terminal-layout', TerminalLayout);
Vue.component('empty-layout', EmptyLayout);
Vue.component('main-layout', MainLayout);

Vue.component('input-spinner', InputSpinner);
Vue.component('right-panel', RightPanel);
Vue.component('success-view', SuccessView);
Vue.component('error-view', ErrorView);
Vue.component('ModalWrapper', ModalWrapper)
Vue.component('InputMasked', InputMasked);

const config = require('./main/config/app.config');
sync(store, router)

/**
 * Set global config
 */
Vue.config.productionTip = false;
Vue.config.devtools = config.debug;
Vue.config.debug = config.debug;

/**
 * Bind global config app in store
 */
Config.bind(store, config);

/**
 * Init app
 */
requests.config.getConfig()
  .then((response) => {
    store.dispatch('setCurrenciesByTypes', response.data.currencies_by_types);
    store.dispatch('setCurrencies', response.data.currencies);
    new Vue({
      render: h => h(App),
      el: '#app',
      // router: maintenanceRouter,
      router,
      store,
      created() {
        if (this.$route.query.r) {
          this.$cookies.set(
            'referral',
            this.$route.query.r,
            '1d'
          );
        }
      },
      computed: mapGetters({
        isAuthenticated: 'isAuthenticated'
      }),
      onIdle() {
        // Logout user when idle 15 minutes
        if (this.isAuthenticated) {
          this.$store.dispatch('logout').then(() => {
            document.location.reload()
          })
        }
      },
      mounted() {
        if (this.isAuthenticated) {
          store.dispatch('access')
        }
      }
    })
  })
  .catch((e) => {
    new Vue({
      render: h => h(App),
      el: '#app',
      router: maintenanceRouter,
      store,
      created() {
        this.$store.dispatch('logout')
        this.$store.dispatch('setMaintenanceStatus', true)
      },
    })
  })
