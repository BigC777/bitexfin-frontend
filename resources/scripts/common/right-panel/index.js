import jQuery from 'jquery'

export default {
  name: 'right-panel',
  data(){
    return{
      isShow:false
    }
  },
  props: {
    title: {
      type: String,
      require: true
    },
    mode: {
      type: String,
      default: 'not-full'
    },
    spinner: {
      type: Object,
      default: () => {
        return {
          message: 'Loading...',
          shown: false
        };
      },
    },
  },
  methods: {
    show() {
      this.toggle(true)
    },
    hide(time = 10) {
      setTimeout(() => {
        this.toggle(false);
        this.spin(false);
        this.$emit('close');
      }, time);
    },
    toggle(visible) {
      jQuery('html').toggleClass('overflow-hidden', visible)
      this.isShow = visible;
    },
    spin(show = true) {
      this.spinner.shown = show
    }
  },
  template: `
    <div :class="['right-popup', { 'right-popup__full-screen': mode === 'full' }, { 'right-popup-shown': isShow }]">
      <div class="right-popup__background"></div>
      <div :class="['right-popup__spinner-wrapper', { 'd-block': spinner.shown }]">
        <div class="right-popup__spinner">
          <div class="spinner-inner">
            <i class="fas fa-circle-notch fa-spin"></i>
            <div>{{ spinner.message }}</div>
          </div>
        </div>
      </div>
      <div class="right-popup__content">
        <div class="right-popup__close">
          <i class="far fa-times-circle" @click="hide"></i>
        </div>
        <div class="right-popup__main">
          <div class="popup-content">
            <div class="right-menu__header">
              <i class="fas fa-home"></i>
              <span class="right-menu__header__text">{{ title }}</span>
            </div>
            <slot></slot>
          </div>
        </div>
      </div>
    </div>
  `
}
