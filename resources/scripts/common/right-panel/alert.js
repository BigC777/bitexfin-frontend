export default {
  template: `
    <div class="right-popup__clients-info_wrapper d-block">
      <div class="right-menu__header">
        <i class="fas fa-home"></i>
        <span class="right-menu__header__text">Success</span>
      </div>
      <div class="right-menu__form__text">Operation completed successfully</div>
    </div>
  `
}