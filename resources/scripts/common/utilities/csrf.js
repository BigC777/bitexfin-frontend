export default class Csrf {
  static mergeObjectWithCsrf(object) {
    return {...object, ..._appGlobal.csrf}
  }

  static appendCsrfToFormData(formData) {
    Object.keys(_appGlobal.csrf).forEach(key => formData.append(key, _appGlobal.csrf[key]));
    return formData;
  }

  static setCsrf(fromRemote = false, csrf = null) {
    if (!fromRemote) {
      csrf = document.getElementsByTagName('meta')['csrf-token'].getAttribute("content");
      csrf = JSON.parse(csrf);
    }
    window._appGlobal = {
      csrf: csrf
    }
  }

  static getCsrf() {
    return _appGlobal.csrf;
  }

  static prepareResponseWithCsrf(response) {
    if (typeof response !== 'object') {
      return;
    }

    let csrfExist = Object.keys(response).some(v => v == 'csrf');

    if (!csrfExist) {
      return;
    }

    this.setCsrf(true, response.csrf)
  }
}
