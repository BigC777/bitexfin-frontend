import _ from 'underscore'

export default class Config {
  static bind(store, options) {
    _.map(options, (value, attribute) => {
      store.commit('setAttribute', {
        attribute: attribute,
        value: value
      });
    });
  }
};
