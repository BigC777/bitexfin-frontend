import jQuery from 'jquery'

export default {
  toggle(isShow = true) {
    jQuery('html').toggleClass(
      'run-ajax',
      isShow
    )
  }
}
