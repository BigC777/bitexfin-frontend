import jQuery from "jquery";

export default (elements, errors) => {
  if (typeof errors === 'string') {
    return [errors]
  }

  if (typeof errors[Object.keys(errors)] === 'string') {
    return [errors[Object.keys(errors)]]
  }

  Object.keys(errors).map(key => {
    Object.values(errors[key]).map(error => {
      if (error !== '') {
        jQuery(elements['error_' + key]).text(error)
        jQuery(elements['field_' + key]).addClass('is-invalid')
      }
    })
  })

  return []
}