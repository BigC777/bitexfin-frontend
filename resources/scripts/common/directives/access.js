import {mapGetters} from 'vuex'
import _ from 'underscore'

export default {
  inserted(element) {
    element.style.display = 'none';
  },
  update(element, {value}, vnode) {
    _.each(value.can, item => {
      _.filter(value.permissions, function (permission) {
        if (permission.path === item) {
           element.style.display = '';
        }
      });
    });
  }
}