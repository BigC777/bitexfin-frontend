import Swal from 'sweetalert2'

//docs https://sweetalert2.github.io/#configuration

const defaultSuccessAlert = Swal.mixin({
  toast: true,
  position: 'center',
  showConfirmButton: false,
  animation:false,
  timer: 5000,
  type: 'success',
  title: '',
  text:''
});
const defaultErrorAlert = Swal.mixin({
  toast: true,
  showConfirmButton: false,
  showCancelButton: true,
  cancelButtonText:'OK',
  cancelButtonClass:'btn btn-primary',
  animation:false,
  type: 'error',
});

export default {
  success(config) {
    return defaultSuccessAlert.fire(config)
  },
  error(config) {
    return defaultErrorAlert.fire(config)
  },
  close () {
    return defaultErrorAlert.close()
  }
}
