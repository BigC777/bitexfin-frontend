import requests from "../admin/requests"
import requests_main from "../main/requests"
import {handlerErrors} from '../main/utilities'
export default {
  template: `
    <div>
      <div class="media" v-if="url">
        <img class="mr-3" :src="url"/>
        <div class="media-body">
          <h5 class="mt-0">QR-code</h5>Сканируйте QR-code через приложение Google Authenticator</div>
      </div>
      <div class="right-menu__form__row mt-2">
          <input type="text"
             class="right-menu__form__input"
             @input="$emit('ga_code', $event.target.value)"
             placeholder="GAuth code"
             ref="field_ga_code">
          <button v-if="(this.env === 'dashboard')" 
             type="button" class="btn btn-link" @click="resetAuth">Get QR-code</button>
      </div>
      <div class="text-danger error-msg" ref="error_ga_code"></div>
    </div>
  `,
  name: "Google2Fa",
  data () {
    return {
      url: null,
      ga_code: null,
      true_requests: null,
      errors: null,
    }
  },
  props: {
    env: String
  },
  methods: {
    resetAuth() {
        this.errors = [];
        this.true_requests.resetForm({})
        .then(({data}) => {
          if (data.data.url !== '') {
            this.url = data.data.url
          }
        })
        .catch(({response}) => {
          if ((response !== undefined) && response.data) {
            this.errors = handlerErrors(this.$refs, response.data.errors)
          }
        })
      }
  },
  mounted() {
    if (this.env === undefined) {
        this.true_requests = requests.google2fa
    } else if (this.env === 'dashboard') {
        this.true_requests = requests_main.dashboard.google2fa
    }
    if (this.env !== 'authenticate') {
        this.true_requests.getForm({})
            .then(({data}) => {
                if (data.data.url !== '') {
                    this.url = data.data.url
                }
            })
            .catch(({response}) => {
                if ((response !== undefined) && response.data) {
                    this.errors = handlerErrors(this.$refs, response.data.errors)
                }
            })
    }
    this.$refs.error_ga_code.innerHTML = ''
    this.$refs.field_ga_code.classList.remove("is-invalid")
  }
}