export default {
  install (Vue, options) {
    const { store } = options || {}
    const events = ['online']

    events.forEach(event => window.addEventListener(event, () => {
      if (store.getters.isAuthenticated) {
        store.dispatch('logout').then(() => {
          document.location.reload()
        })
      }
    }));
  }
}
