import requests from "../../admin/requests";

export default {
  state: {
    bonusPercentsOnAccountSettings: [],
    bonusPercentsOnAccountUnusedCurrencies: [],
    allCurrencies:[]
  },
  getters: {
    getBonusPercentsOnAccountSettings: state => state.bonusPercentsOnAccountSettings,
    getBonusPercentsOnAccountUnusedCurrencies: state => state.bonusPercentsOnAccountUnusedCurrencies,
  },
  mutations: {
    setBonusPercentsOnAccountSettings(state, settings) {
      state.bonusPercentsOnAccountSettings = settings;
      state.bonusPercentsOnAccountUnusedCurrencies = state.allCurrencies.filter(currency => {
        return state.bonusPercentsOnAccountSettings.find(setting => setting.currency === currency) === undefined
      })
    },
    setCurrencies(state, currencies) {
      state.allCurrencies = currencies
    }

  },
  actions: {
    loadBonusPercentsOnAccountIndex({commit}) {
      requests.bonusPercentsOnAccount.indexData()
        .then(({data}) => {
          commit('setCurrencies', data.currencies)
          commit('setBonusPercentsOnAccountSettings', data.bonus_percents_on_account_settings)
        })
        .catch(({response}) => console.error(response))
    },
    updateBonusPercentsOnAccountSettingsList({commit}, settings) {
      commit('setBonusPercentsOnAccountSettings', settings)
    },
  }
}
