import Access from './access'
import TerminalSettings from './terminal-settings'
import BonusPercentsOnAccount from './bonus-percents-on-account'

import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Access,
    TerminalSettings,
    BonusPercentsOnAccount
  }
})