import requests from "../../admin/requests";

export default {
  state: {
    permissions: []
  },
  getters: {
    getPermissions: state => state.permissions,
  },
  mutations: {
    setPermissions(state, permissions) {
      state.permissions = permissions;
    }
  },
  actions: {
    loadPermissions({commit}) {
      requests.access.getPermissions()
        .then(({ data }) => commit('setPermissions', data.permissions))
        .catch(({ response }) => console.error(response))
    }
  }
}
