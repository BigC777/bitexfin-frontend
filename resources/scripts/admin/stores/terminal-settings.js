import requests from "../../admin/requests";

export default {
  state: {
    auxiliaryOrders: [],
    auxiliaryOrdersUnusedPairs: [],
    allPairs: [
      'BTC-GBP',
      'BTC-EUR',
      'BTC-USD',
      'BTC-USDT',
      'BTC-RUB',
      'BTC-KZT',

      'ETH-EUR',
      'ETH-GBP',
      'ETH-USD',
      'ETH-USDT',
      'ETH-RUB',
      'ETH-KZT',
      'ETH-BTC',

      'USDT-USD',
      'USDT-RUB',
      'USDT-EUR',
      'USDT-GBP',
      'USDT-KZT',

      'CFX-EUR',
      'CFX-GBP',
      'CFX-USD',
      'CFX-RUB',
      'CFX-KZT',
      'USDT-CFX',
      'BTC-CFX',
      'ETH-CFX',

      'USC-RUC',
      'EUC-RUC'
    ],
    rateCorrectionSettings: [],
    rateCorrectionUnusedPairs: [],
    activeOrderBookTemplates:[]
  },
  getters: {
    getAuxiliaryOrders: state => state.auxiliaryOrders,
    getAuxiliaryOrdersUnusedPairs: state => state.auxiliaryOrdersUnusedPairs,
    getRateCorrectionSettings: state => state.rateCorrectionSettings,
    getRateCorrectionUnusedPairs: state => state.rateCorrectionUnusedPairs,
    getActiveOrderBookTemplates: state => state.activeOrderBookTemplates,
    getPairs: state=>state.allPairs
  },
  mutations: {
    setAuxiliaryOrders(state, orders) {
      state.auxiliaryOrders = orders;
      state.auxiliaryOrdersUnusedPairs = state.allPairs.filter(pair => state.auxiliaryOrders.find(order => order.pair === pair) === undefined)
    },
    setRateCorrectionSettings(state, rateCorrectionSettings) {
      state.rateCorrectionSettings = rateCorrectionSettings;
      state.rateCorrectionUnusedPairs = state.allPairs.filter(pair => state.rateCorrectionSettings.find(setting => setting.pair === pair) === undefined)
    },
    setActiveOrderBookTemplates(state, activeOrderBookTemplates){
    if(Array.isArray(activeOrderBookTemplates)){
        state.activeOrderBookTemplates = activeOrderBookTemplates
      }else{
        state.activeOrderBookTemplates.push(activeOrderBookTemplates)
      }
    }

  },
  actions: {
    loadTerminalSettingsIndex({commit}) {
      requests.terminalSettings.indexData()
        .then(({data}) => {
          commit('setAuxiliaryOrders', data.auxiliary_orders)
          commit('setRateCorrectionSettings', data.rate_correction_settings)
          commit('setActiveOrderBookTemplates', data.active_order_book_templates)
        })
        .catch(({response}) => console.error(response))
    },
    updateAuxiliaryOrdersList({commit}, orders) {
      commit('setAuxiliaryOrders', orders)
    },
    updateRateCorrectionSettingsList({commit}, rateCorrectionSettings) {
      commit('setRateCorrectionSettings', rateCorrectionSettings)
    },
    updateActiveOrderBookTemplatesList({commit}, activeOrderBookTemplates){
      commit('setActiveOrderBookTemplates', activeOrderBookTemplates)
    }
  }
}
