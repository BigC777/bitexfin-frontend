import axios from 'axios'
import Csrf from '../common/utilities/csrf'

const instance = axios.create()

instance.interceptors.response.use(
  response => {
    Csrf.prepareResponseWithCsrf(response.data);
    return response;
  },
  error => {
    Csrf.prepareResponseWithCsrf(error.response.data);
    return Promise.reject(error);
  }
)

export default {
  authenticate: {
    login(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/login', body);
    },
    logout() {
      let body = Csrf.mergeObjectWithCsrf({});
      return instance.post('/logout', body);
    },
  },
  bankRequisites: {
    approve(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/bank_requisites/approve', body)
    },
    discard(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/bank_requisites/discard', body)
    },
  },
  currencies: {
    postAddNewCurrency(body) {
      body = Csrf.appendCsrfToFormData(body);
      return instance.post('/currencies', body)
    },
    postChangeCurrency(currencyId, body) {
      body = Csrf.appendCsrfToFormData(body);
      return instance.post(`/currencies/${currencyId}`, body)
    },
    delete(currencyId) {
      let body = Csrf.mergeObjectWithCsrf({});
      return instance.delete(`/currencies/${currencyId}`,{ data: body } )
    },
    getAllCurrencies(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.get('/currencies/get-system-currencies')
    }
  },
  user: {
    update (userId, body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.put('/users/' + userId, body)
    },
    updateRequisites (userId, body) {
      body = Csrf.appendCsrfToFormData(body);
      return instance.post('/users/' + userId +'/requisites', body)
    },
    updateCards (userId, body) {
      body = Csrf.appendCsrfToFormData(body)
      return instance.post('/users/' + userId + '/cards', body)
    },
    updateSettings (userId, body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.put('/users/' + userId +'/settings', body)
    },
    sendMail (body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/users/send-mail', body)
    },
    refill (userId, body) {
      body = Csrf.mergeObjectWithCsrf(body)
      return instance.post('/users/' + userId + '/refill', body)
    },
    tokenAddress (userId, body) {
      body = Csrf.mergeObjectWithCsrf(body)
      return instance.post('/users/' + userId + '/token-address', body)
    }
  },
  exchangeAccount: {
    list() {
      return instance.get('/exchange-account/list')
    },
    create(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/exchange-account/save', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    update(accountId, body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.put('/exchange-account/' + accountId, body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    remove(accountId, body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/exchange-account/' + accountId + '/delete', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
  },
  voucherSettings: {
    list() {
      return instance.get('/voucher-settings/list')
    },
    update(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.put('/voucher-settings/', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    remove(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/voucher-settings/delete', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
  },
  vouchers: {
    remove(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/vouchers/delete', body)
    },
    get(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/vouchers/get', body)
    },
  },
  card: {
    discard (body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/cards/discard', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    approve (body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/cards/approve', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    }
  },
  replenishments: {
    list(params) {
      return instance.get('/replenishments/list', {
        params
      });
    },
    accept(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/replenishments/accept', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    discard(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/replenishments/discard', body);
    }
  },
  limits: {
    create(limit) {
      limit = Csrf.mergeObjectWithCsrf(limit);
      return instance.post('/accounts/create', limit);
    },
    update(limit) {
      limit = Csrf.mergeObjectWithCsrf(limit);
      return instance.post(`/accounts/${limit.id}`, limit);
    },
    updateUserLimits(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/accounts/users/update', body);
    },
    getUserLimits(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/accounts/users/get-by-id', body);
    }
  },
  transaction: {
    list(params) {
      return instance.get('/transactions/list', {
        params
      });
    },
    getTypes() {
      return instance.get('/transactions/indexes')
    }
  },
  access: {
    getPermissions() {
      return instance.get('/permissions');
    }
  },
  systemBankRequisites: {
    list() {
      return instance.get('/system-bank-requisites/list')
    },
    update(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/system-bank-requisites/update', body);
    }
  },
  withdrawals: {
    list (params) {

      return instance.get('/withdrawals/list', {
        params
      })
    },
    discard (body) {
      body = Csrf.mergeObjectWithCsrf(body)

      return instance.post('/withdrawals/discard', body)
    },
    accept (body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body)

      return instance.post('/withdrawals/accept', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    setPaid (body) {
      body = Csrf.mergeObjectWithCsrf(body)

      return instance.post('/withdrawals/set-paid', body)
    }
  },
  terminalSettings:{
    indexData() {
      return instance.get('/terminal-settings/index');
    },
    createAuxiliaryOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/create-auxiliary-order', body);
    },
    updateAuxiliaryOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/update-auxiliary-order', body);
    },
    deleteAuxiliaryOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/delete-auxiliary-order', body);
    },
    createRateCorrection(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/create-rate-correction', body);
    },
    updateRateCorrection(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/update-rate-correction', body);
    },
    deleteRateCorrection(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/delete-rate-correction', body);
    },
    createOrderBookTemplate(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/create-order-book-template', body);
    },
    updateOrderBookTemplate(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/update-order-book-template', body);
    },
    deleteOrderBookTemplate(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/terminal-settings/delete-order-book-template', body);
    },
  },
  exchangeOrderSettings:{
    indexData() {
      return instance.get('/external-orders/index');
    },
    createExchangeOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/external-orders/create', body);
    },
    updateExchangeOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/external-orders/update', body);
    },
    deleteExchangeOrder(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/external-orders/delete', body);
    }
  },
  bonusPercentsOnAccount:{
    indexData() {
      return instance.get('/bonus-percents-on-account/index');
    },
    createBonusPercentsOnAccount(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/bonus-percents-on-account/create', body);
    },
    updateBonusPercentsOnAccount(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/bonus-percents-on-account/update', body);
    },
    deleteBonusPercentsOnAccount(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/bonus-percents-on-account/delete', body);
    }
  },
  google2fa:{
    getForm() {
      return instance.get('/google-auth/form');
    },
    resetForm() {
      return instance.get('/google-auth/reset');
    },
  },
  actionsLog:{
    list(body) {
      //body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/user-actions-log/list', body);
    },
  },
  partners: {
    tariffs: {
      create (getCode, body) {
        return instance.post('/partners/tariffs/create', {...body}, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        })
      },
      update (getCode, body) {
        return instance.post('/partners/tariffs/update', {...body}, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        })
      },
      delete (getCode, body) {
        return instance.post('/partners/tariffs/delete', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        })
      }
    }
  },
  userVoucherSettings:{
    getUserVoucherSettings(body){
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/voucher-settings/users/get-by-id', body);
    },
    updateUserVoucherSetting(body){
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/voucher-settings/users/update', body);
    }
  },
  emailDelivery:{
    getAllUsers:()=>{
      return instance.get('/email-delivery/get-users');
    },
    getEmailDeliveries:()=>{
      return instance.get('/email-delivery/get');
    },
    postEmailDelivery:(body)=>{
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/email-delivery/post', body);
    },
  },
  serviceManagement:{
    getSettings:()=>{
      return instance.get('/service-management/get');
    },
    postCommands:(body)=>{
      return instance.post('/service-management/post', body);
    },
  }
}
