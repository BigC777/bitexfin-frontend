const config = {
  debug: false,
  appUrl: 'https://bitexfin.com/',
  socketUrl: 'wss://bitexfin.com/socket/ws/',
  orderApiUrl: 'https://bitexfin.com/order/api',
  historyApiUrl: 'https://bitexfin.com/history/api',
  historyApiWsUrl: 'wss://bitexfin.com/history/ws/',
};

module.exports = {
  ...config,
  ...require('./app-local.config')
};