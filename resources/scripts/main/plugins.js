export default {
  install(Vue, options) {
    Vue.prototype.$scrollBody = state => document.body.style.overflow = state
  }
}