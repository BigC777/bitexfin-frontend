import store from './stores/index'
import user from './stores/modules/user'
import axios from 'axios'
import Csrf from '../common/utilities/csrf'

const instance = axios.create({
  headers: {
    // Authorization: localStorage.getItem('token'),
    bitexfinLanguage: user.state.selectedLang
  }//,
  // baseURL: 'https://bitexfin.com/'
})

instance.interceptors.response.use(
  response => {
    Csrf.prepareResponseWithCsrf(response.data);
    return response;
  },
  error => {
    Csrf.prepareResponseWithCsrf(error.response.data);
    if (error.response.status === 401) {
      store.commit('logout')
      if(document.location.pathname!=='/') {
        document.location.href = '/'
      }
    }

    if ([408, 500, 504].includes(error.response.status) || error.code === 'ECONNABORTED') {
      setTimeout(()=>{
        if(!store.getters.getMaintenanceStatus){
          document.location.reload()
        }
      },2000)
    }

    return Promise.reject(error)
  }
)

export default {
  config: {
    getConfig() {
      return new Promise((resolve, reject)=>{
        instance.get('/config.json', {}).then((response)=>{
          Csrf.prepareResponseWithCsrf(response.data);
          resolve(response)
        })
        .catch((error)=>{
          reject(error)
        })
      })
    }
  },
  check: {
    access() {
      return instance.get('/api/v1/check-access')
    }
  },
  authenticate: {
    login(data, getCode = false) {
      data = Csrf.mergeObjectWithCsrf(data);
      return instance.post('/api/v1/login', data, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    logout() {
      // return instance.post('/api/v1/logout')
      return true;
    },
    register(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/register', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    confirmEmail(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/send-email-confirm', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
    captchaValidation(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/captcha', body)
    },
    postExistsUserByAttribute(body) {
      return instance.post('/api/v1/exists-user-by-attribute', body)
    },
    postRequestResetPasswordCode(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/reset-password/code-verify', body)
    },
    postCheckCode(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/reset-password/check-code', body)
    },
    postResetPassword(body, getCode = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/reset-password', body, {
        params: {
          getCode: getCode ? '1' : '0',
        }
      })
    },
  },
  bitexcard: {
    activate(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/bitexcard/activate', body)
    },
    shop(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/bitexcard/shop', body)
    },
    check(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/bitexcard/check', body)
    },
  },
  dashboard: {
    getIndexData() {
      return instance.get('/api/v1/dashboard')
    },
    getTransactions(currency = null) {
      return instance.get('/api/v1/dashboard/user-account-transactions?currency=' + currency)
    },
    getChartsData() {
      return instance.get('/api/v1/dashboard/charts-data')
    },
    postNewCard(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/cards/new', body)
    },
    userAgree() {
      return instance.get('/api/v1/dashboard/agree')
    },
    getNewCard() {
      return instance.get('/api/v1/dashboard/cards/new')
    },
    deleteCard(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/cards/delete', body);
    },
    getExternalBill(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.get(`/api/v1/external_bill/${body.hash}`, body)
    },
    paymentExternalBill(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post(`/api/v1/external_bill/payment`, body)
    },
    postRefillPay(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/refill/new', body)
    },
    postNewCryptoAddress(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/crypto-requisites/new', body)
    },
    postDownloadReceipt(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/refill/receipt', body, {responseType: 'blob'})
    },
    postDownloadCertificate(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/refill/certificate', body, {responseType: 'blob'})
    },
    postWithdrawalPay(body, getCode = false, getErrors = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/withdrawal/new', body, {
        params: {
          getCode: getCode ? '1' : '0',
          getErrors: getErrors ? '1' : '0',
        }
      })
    },
    postWithdrawalVoucherPay(body, getCode = false, getErrors = false) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/profile/create-withdrawal-voucher', body, {
        params: {
          getCode: getCode ? '1' : '0',
          getErrors: getErrors ? '1' : '0',
        }
      })
    },
    getUnreadNotifications() {
      return instance.get('/api/v1/dashboard/notifications/list');
    },
    removeNotification(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/notifications/change-status', body)
    },
    getSystemBankRequisites() {
      return instance.get('/api/v1/dashboard/bank-requisites/system-requisites')
    },
    deleteBankRequisite(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/bank-requisites/delete', body);
    },
    getUserTransactions(currency, offset = 0) {
      return instance.get('/api/v1/dashboard/transactions/' + currency + '?offset=' + offset)
    },
    getUserTransactionsAll(offset = 0) {
      return instance.get('/api/v1/dashboard/transactions_all?page=' + offset)
    },
    getUserExternalBills(offset = 0) {
      return instance.get('/api/v1/dashboard/external_bills?offset=' + offset)
    },
    getUserExternalBillsByHash(hash) {
      return instance.get('/api/v1/dashboard/external_bills_by_hash?hash=' + hash)
    },
    cancelExternalBill(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/dashboard/external_bill/cancel', body)
    },

    google2fa: {
      getForm() {
        return instance.get('/api/v1/dashboard/profile/security/google-auth/form');
      },
      resetForm() {
        return instance.get('/api/v1/dashboard/profile/security/google-auth/reset');
      },
    },

    profile: {
      getUserData() {
        return instance.get('/api/v1/dashboard/user')
      },
      getUserCards() {
        return instance.get('/api/v1/dashboard/user-cards')
      },
      getUserBankRequisites() {
        return instance.get('/api/v1/dashboard/user-bank-requisites')
      },
      getUserCryptoRequisites() {
        return instance.get('/api/v1/dashboard/user-crypto-requisites')
      },
      postChangeLang(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-user-lang', body);
      },
      postPrivateData(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-private-data', body);
      },
      postChangeAddress(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-address', body);
      },
      postChangePassport(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-passport', body);
      },
      postChangePhoneAndEmail(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-phone-and-email', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        });
      },
      postChangePhone(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-phone', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        })
      },
      postChangeEmail(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-email', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        })
      },
      postConfirmationOfChangeIdentity(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/confirmation-of-change-identity', body, {
          params: {
            getCode: getCode ? '1' : '0'
          }
        })
      },
      postConfirmationOfChangeEmailIdentity(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/confirmation-of-change-email-identity', body, {
          params: {
            getCode: getCode ? '1' : '0'
          }
        })
      },
      postChangeCard(body) {
        body = Csrf.appendCsrfToFormData(body);
        return instance.post('/api/v1/dashboard/profile/change-card', body);
      },
      postChangeBankRequisite(body, getCode = false) {
        body = Csrf.appendCsrfToFormData(body);
        return instance.post('/api/v1/dashboard/profile/change-bank-requisite', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        });
      },
      postChangeCryptoRequisites(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-crypto-requisite', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        });
      },
      postChangePassword(body, getCode = false) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/change-password', body, {
          params: {
            getCode: getCode ? '1' : '0',
          }
        });
      },
      getDocuments() {
        return instance.get('/api/v1/dashboard/profile/documents');
      },
      postDocuments(body) {
        body = Csrf.appendCsrfToFormData(body);
        return instance.post('/api/v1/dashboard/profile/documents', body);
      },
      appendDocuments(body) {
        body = Csrf.appendCsrfToFormData(body);
        return instance.post('/api/v1/dashboard/profile/documents/append', body);
      },
      removeDocument(body) {
        return instance.post('/api/v1/dashboard/profile/documents/remove', body)
      },
      getInvestTariffs() {
        return instance.get('/api/v1/dashboard/profile/invest-tariffs');
      },
      getInvestsList() {
        return instance.get('/api/v1/dashboard/profile/invests-list');
      },
      getUserInvestAccountsForUser() {
        return instance.get('/api/v1/dashboard/profile/get-user-invest-accounts-for-user');
      },
      getUserInvestTransactionsByUserInvestAccountId(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/get-user-invest-transactions-by-user-invest-account-id', body);
      },
      getInvesById(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/get-invest-by-id', body);
      },
      preActivateInvestment(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/pre-activate-investment', body);
      },
      postNewInvest(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/new-invest', body);
      },
      postCloseInvest(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/close-invest', body);
      },
      postAddNewMemberToInvest(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/add-new-member-to-invest', body);
      },
      postPaymentOfInvestment(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/payment-of-investment', body);
      },
      postExchangeInvestment(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post('/api/v1/dashboard/profile/exchange-investment', body);
      },
      getBankRequisitesDocuments() {
        return instance.get('/api/v1/dashboard/bank-requisites/documents');
      },
      verifications: {
        postChangeVerificationStatus() {
          let body = Csrf.mergeObjectWithCsrf({});
          return instance.post('/api/v1/dashboard/profile/verifications/change-status', body);
        }
      },
      security: {
        postTwoFactorAuth(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/security/two-factor-auth', body);
        },
        postAddAllowedIp(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/security/add-allowed-ip', body);
        },
        deleteAllowedIp(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/security/delete-allowed-ip', body);
        }
      },
      settings: {
        postNotifySettings(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/settings', body);
        }
      },
      authHistory: {
        getAuthHistory(offset = 0) {
          return instance.get('/api/v1/dashboard/profile/auth-history?offset=' + offset)
        },
      },
      qrCode: {
        qrCode(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/refill/notify-external-payer', body);
        },
      },
      vouchers: {
        postCreateVoucher(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/create-voucher', body);
        },
        preActivateVoucher(hash) {
          return instance.get('/api/v1/dashboard/profile/pre-activate-voucher?hash=' + hash);
        },
        activateVoucher(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/activate-voucher', body);
        },
        sendVoucher(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/send-voucher', body);
        },
        postCancelVoucher(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/cancel-voucher', body);
        },
      },
      accountsSettings: {
        getIndexData() {
          return instance.get('/api/v1/dashboard/profile/accounts-settings');
        },
        postChangeStatusPercentBonusAccount(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/accounts-settings/change-percent-bonus-account-status', body);
        }
      },
      apiKeys: {
        get() {
          return instance.get('/api/v1/dashboard/profile/api-keys');
        },
        create(body, getCode) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/api-keys/create', body, {
            params: {
              getCode: getCode ? '1' : '0',
            }
          });
        },
        delete(body) {
          body = Csrf.mergeObjectWithCsrf(body);
          return instance.post('/api/v1/dashboard/profile/api-keys/delete', body, {});
        }
      }
    },
    partners: {
      get(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.get(`/api/v1/dashboard/partners/${body.partner_bonus_type}`, body)
      },
      getSelfBonus(body) {
        body = Csrf.mergeObjectWithCsrf(body);
        return instance.post(`/api/v1/dashboard/partners/get_bonus`, body)
      }
    }
  },
  exchange: {
    getIndexData() {
      return instance.get('/api/v1/exchange')
    },
    postIndexData(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/exchange', body)
    },
    postNewTokenClaim(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/exchange/token-claim', body)
    },
    generateUserTokenAddress() {
      return instance.get('/api/v1/exchange/generate-token-address')
    },
    postBonus(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/exchange/bonus', body)
    }
  },
  fees: {
    getSystemLimits() {
      return instance.get('/api/v1/fees');
    }
  },
  terminal: {
    getExchangePairs() {
      return instance.get('/api/v1/terminal/pairs')
    },
    getIndex(pair) {
      return instance.get('/api/v1/terminal/orders/index' + '?pair=' + pair)
    },
    sendEmail(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/terminal/send-email', body)
    },
    postOrderData(body) {
      return instance.post(store.getters.getOrderApiUrl + '/create', body)
    },
    postCancel(body) {
      return instance.post(store.getters.getOrderApiUrl + '/cancel', body)
    }
  },
  score: {
    postIndexData(body) {
      body = Csrf.mergeObjectWithCsrf(body);
      return instance.post('/api/v1/score', body)
    }
  },
  madmeal: {
    getMadMeal() {
      return instance.get('/api/v1/madmeal')
    }
  }
}
