import PaymentButtonWidget from '../views/PaymentButtonWidget.vue'

export default {
  component: PaymentButtonWidget,
  name: 'paymentButtonWidget',
  path: '/external_payment/:hash/:err?',
  meta: {
    layout: 'empty-layout'
  }
}
