import Terminal from '../../views/terminal/Index'

export default {
  component: Terminal,
  name: 'terminal-index',
  path: '/terminal',
  meta: {
    layout: 'terminal-layout',
    requiresAuth: false
  }
}
