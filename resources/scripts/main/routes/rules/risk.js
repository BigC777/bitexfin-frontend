import Risk from '../../views/rules/Risk.vue'

export default {
  component: Risk,
  name: 'risk',
  path: '/rules/risk',
  meta: {
    layout: 'main-layout'
  }
}
