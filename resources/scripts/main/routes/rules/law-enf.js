import LawEnf from '../../views/rules/LawEnf.vue'

export default {
  component: LawEnf,
  name: 'law-enf',
  path: '/rules/law-enf',
  meta: {
    layout: 'main-layout'
  }
}
