import Transactions from '../../views/transactions/Index'

export default {
	component: Transactions,
	name: 'transactions',
	path: '/transactions',
	meta: {
		layout: 'new-dashboard-layout',
		requiresAuth: true
	}
}