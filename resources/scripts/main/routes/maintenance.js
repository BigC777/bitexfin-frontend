import Maintenance from '../views/Maintenance.vue'

export default {
  component: Maintenance,
  name: 'maintenance',
  path: '*',
  meta: {
    layout: 'main-layout'
  }
}
