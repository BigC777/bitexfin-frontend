import LoginContinue from '../views/LoginContinue.vue'

export default {
  component: LoginContinue,
  name: 'loginContinue',
  path: '/external_bill/:hash/:err?',
  meta: {
    layout: 'main-layout'
  }
}
