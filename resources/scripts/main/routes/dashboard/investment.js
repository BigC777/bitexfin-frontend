import Profile from '../../views/dashboard/Investment.vue'

export default {
  component: Profile,
  name: 'dashboard-investment',
  path: '/dashboard/investment',
  meta: {
    layout: 'new-dashboard-layout',
    requiresAuth: true
  }
}
