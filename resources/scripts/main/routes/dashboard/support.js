import Support from '../../views/dashboard/Support.vue'

export default {
  component: Support,
  name: 'dashboard-support',
  path: '/dashboard/support',
  meta: {
    layout: 'dashboard-layout',
    requiresAuth: true
  }
}
