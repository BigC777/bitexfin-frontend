import Dashboard from '../../views/dashboard/Index.vue'

export default {
  component: Dashboard,
  name: 'dashboard-index',
  path: '/dashboard',
  meta: {
    layout: 'new-dashboard-layout',
    requiresAuth: true
  }
}
