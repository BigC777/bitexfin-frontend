import Profile from '../../views/dashboard/Profile.vue'

export default {
  component: Profile,
  name: 'dashboard-profile',
  path: '/dashboard/profile',
  meta: {
    layout: 'new-dashboard-layout',
    requiresAuth: true
  }
}
