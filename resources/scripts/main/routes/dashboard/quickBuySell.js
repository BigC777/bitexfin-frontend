import Dashboard from '../../views/dashboard/QuickBuySell.vue'

export default {
  component: Dashboard,
  name: 'quick-buy-sell-index',
  path: '/quick-buy-sell',
  meta: {
    layout: 'new-dashboard-layout',
    requiresAuth: true
  }
}
