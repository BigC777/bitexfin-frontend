import Activate from '../views/Activate.vue'

export default {
  component: Activate,
  name: 'Activate',
  path: '/activate',
  meta: {
    layout: 'main-layout'
  }
}
