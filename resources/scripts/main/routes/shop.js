import Activate from '../views/Shop.vue'

export default {
  component: Activate,
  name: 'Shop',
  path: '/shop',
  meta: {
    layout: 'main-layout'
  }
}
