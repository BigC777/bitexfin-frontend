import How from '../../views/main/How.vue'

export default {
  component: How,
  name: 'how-index',
  path: '/how',
  meta: {
    layout: 'main-layout'
  }
}
