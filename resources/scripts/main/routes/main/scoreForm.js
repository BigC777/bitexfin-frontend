import ScoreForm from '../../views/main/ScoreForm.vue'

export default {
  component: ScoreForm,
  name: 'score-form',
  path: '/score',
  meta: {
    layout: 'main-layout'
  }
}
