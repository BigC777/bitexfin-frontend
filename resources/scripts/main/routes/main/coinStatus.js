import CoinStatus from '../../views/main/CoinStatus.vue'

export default {
  component: CoinStatus,
  name: 'coin-status',
  path: '/coin-status',
  meta: {
    layout: 'main-layout'
  }
}
