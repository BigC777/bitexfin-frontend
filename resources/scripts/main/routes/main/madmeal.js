import How from '../../views/main/MadMeal.vue'

export default {
  component: How,
  name: 'mad-meal-index',
  path: '/madmeal',
  meta: {
    layout: 'main-layout'
  }
}
