import Fees from '../../views/main/Fees.vue'

export default {
  component: Fees,
  name: 'fees-index',
  path: '/fees',
  meta: {
    layout: 'main-layout'
  }
}
