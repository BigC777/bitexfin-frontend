import Main from '../../views/main/Index.vue'

export default {
  component: Main,
  name: 'main-index',
  path: '/',
  meta: {
    layout: 'main-layout'
  }
}
