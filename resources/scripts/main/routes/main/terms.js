import MainTerms from '../../views/main/Terms.vue'

export default {
  component: MainTerms,
  name: 'main-terms',
  path: '/terms',
  meta: {
    layout: 'main-layout'
  }
}
