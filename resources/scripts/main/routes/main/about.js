import About from '../../views/main/About.vue'

export default {
  component: About,
  name: 'about-index',
  path: '/about',
  meta: {
    layout: 'main-layout'
  }
}
