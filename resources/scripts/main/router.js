// Terminal pages
import TerminalRoute from './routes/terminal/index'
import LoginContinue from './routes/loginContinue'
import paymentButtonWidget from './routes/paymentButtonWidget'
import Activate from './routes/activate.js'
import Shop from './routes/shop.js'

import DashboardRoute from './routes/dashboard/index'
import QuickBuySell from './routes/dashboard/quickBuySell'
import ProfileRoute from './routes/dashboard/profile'
import InvestmentRoute from './routes/dashboard/investment'
import SupportRoute from './routes/dashboard/support'
// Rules pages
import RulesLawEnfRoute from './routes/rules/law-enf'
import RulesRiskRoute from './routes/rules/risk'
// Main pages
import AboutRoute from './routes/main/about'
import FeesRoute from './routes/main/fees'
import HomeRoute from './routes/main/index'
import HowRoute from './routes/main/how'
import MadMealRoute from './routes/main/madmeal'
import TermsRoute from './routes/main/terms'
import Transactions from './routes/transactions/index'
import CoinStatus from './routes/main/coinStatus'
import ScoreForm from './routes/main/scoreForm'
// Errors pages
import NotFound from './routes/not-found'
import Maintenance from "@/main/routes/maintenance"


import requests from './requests'
import store from './stores'

import Router from 'vue-router'
import Vue from 'vue'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  routes: [
    RulesLawEnfRoute,
    RulesRiskRoute,
    TerminalRoute,
    DashboardRoute,
    QuickBuySell,
    ProfileRoute,
    InvestmentRoute,
    SupportRoute,
    AboutRoute,
    FeesRoute,
    HomeRoute,
    TermsRoute,
    NotFound,
    HowRoute,
    MadMealRoute,
    Transactions,
    CoinStatus,
    LoginContinue,
    Activate,
    Shop,
    ScoreForm,
    paymentButtonWidget
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.meta.layout === 'main-layout') {
      return {x: 0, y: 0}
    }
  },
})

router.beforeEach(async (to, from, next) => {
  const isCheckAuth = to.matched.some(record => record.meta.requiresAuth)
  const isUserAuth = store.getters.isAuthenticated

  if (isCheckAuth && !isUserAuth) {
    next('/?login=1&redirectTo=' + window.location.href)
  }
  else {
    next()
  }

})


export const maintenanceRouter = new Router({
  mode: 'history',
  routes: [
    HowRoute,
    RulesRiskRoute,
    RulesLawEnfRoute,
    Maintenance
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.meta.layout === 'main-layout') {
      return {x: 0, y: 0}
    }
  },
})

export default {
  router: router,
  maintenanceRouter: maintenanceRouter
}
