import {Decimal} from 'decimal.js'

const add = (num1, num2, isItMoney = true, precision = 8) => {
  let res = Decimal.add(num1, num2);
  return fixedWithRounding(res, isItMoney, precision)
};
const sub = (num1, num2, isItMoney = true, precision = 8) => {
  let res = Decimal.sub(num1, num2);
  return fixedWithRounding(res, isItMoney, precision)
};
const mul = (num1, num2, isItMoney = true, precision = 8) => {
  let res = Decimal.mul(num1, num2);
  return fixedWithRounding(res, isItMoney, precision)
};
const div = (num1, num2, isItMoney = true, precision = 8) => {
  let res = Decimal.div(num1, num2);
  return fixedWithRounding(res, isItMoney, precision)
};

const fixedWithRounding = (num, isItMoney, precision) => {
  return num.toFixed(precision, isItMoney ? Decimal.ROUND_DOWN : Decimal.ROUND_HALF_UP)
};

const toFixedNumberLength = (num, length) => {
  let stringNumber = num.toString();
  let stringNumberDigitsLength = stringNumber.length - 1;
  let fractionalPart = stringNumber.split('.')[1];
  let fractionalPartLength = typeof fractionalPart === 'undefined' ? 0 : fractionalPart.length;
  let finalAccuracy;
  num = new Decimal(num);
  if (stringNumberDigitsLength < length) {
    finalAccuracy = (length - stringNumberDigitsLength) + fractionalPartLength
  } else if (fractionalPartLength > length) {
    finalAccuracy = fractionalPartLength - (stringNumberDigitsLength - length)
  } else {
    finalAccuracy = 0
  }
  num = num.toFixed(finalAccuracy === length ? finalAccuracy - 1 : finalAccuracy);
  return num
};

export {
  add,
  sub,
  mul,
  div,
  toFixedNumberLength,
  fixedWithRounding
}