import {widget as TvWidget} from './charting_library.min.js'
import socket from './datafeeds/socket.js'
import datafeeds from './datafeeds/datafees.js'
import {mapGetters} from "vuex/types";

export default {
  props: {
    containerId: {
      default: 'tv_chart_container',
      type: String,
    },
    chartsStorageApiVersion: {
      default: '1.1',
      type: String,
    },
    clientId: {
      default: 'custodianfund@gmail.com',
      type: String,
    },
    userId: {
      default: 'custodianfund@gmail.com',
      type: String,
    },
    fullscreen: {
      default: false,
      type: Boolean,
    },
    autosize: {
      default: true,
      type: Boolean,
    },
    theme: {
      default: 'Dark',
      type: String,
    },
  },
  data() {
    return {
      widget: null,
      socket: null,
      datafeeds: new datafeeds(this),
      symbol: null,
      interval: null,
      cacheData: {},
      lastTime: null,
      getBarTimer: null,
      isLoading: true
    }
  },
  computed: {
    ...mapGetters({
      orderPair: 'getOrderPair',
      orderApiUrl: 'getOrderApiUrl',
      token: 'getUserToken'
    }),
  },
  created() {
    this.socket = new socket('ws://localhost:9800/ws',this.token)
    this.socket.doOpen()
    this.socket.on('open', () => {
      //this.socket.send({cmd: 'req', args: [`candle.5M.btcusdt}`, 1440, parseInt(Date.now() / 1000)]})
      this.socket.send({
        event: "SUBSCRIBE_CANDLES",
        interval: 1,
        pair: 'BTC-USD',
        timestamp: Math.floor(new Date().getTime() / 1000),
        count: 1440
      })
    })
    this.socket.on('message', this.onMessage)
  },
  mounted() {
    this.init()
  },
  methods: {
    init(symbol = 'BTC-USD', interval = 1) {

      this.symbol = symbol
      this.interval = interval

      if (!this.widget) {
        const widgetOptions = {
          symbol: this.orderPair,
          // BEWARE: no trailing slash is expected in feed URL
          datafeed: this.datafeeds,
          interval: this.interval,
          container_id: this.containerId,
          theme: this.theme,
          client_id: this.clientId,
          user_id: this.userId,
          fullscreen: this.fullscreen,
          autosize: this.autosize,
          studies_overrides: this.studiesOverrides,
          disabled_features: ['header_symbol_search', 'study_templates'],
          enabled_features: ['use_localstorage_for_settings'],
          library_path: '/assets/scripts/charting_library/',
          locale: 'en',
          debug: false
        };
        this.widget = new TvWidget(widgetOptions);

      }
    },
    sendMessage(data) {
      if (this.socket.checkOpen()) {
        this.socket.send(data)
      } else {
        this.socket.on('open', () => {
          this.socket.send(data)
        })
      }
    },
    unSubscribe(interval) {
      // if (interval < 60) {
      //   this.sendMessage({
      //     cmd: 'unsub',
      //     args: [`candle.M${interval}.${this.symbol.toLowerCase()}`, 1440, parseInt(Date.now() / 1000)]
      //   })
      // } else if (interval >= 60) {
      //   this.sendMessage({
      //     cmd: 'unsub',
      //     args: [`candle.H${interval / 60}.${this.symbol.toLowerCase()}`, 1440, parseInt(Date.now() / 1000)]
      //   })
      // } else {
      //   this.sendMessage({
      //     cmd: 'unsub',
      //     args: [`candle.D1.${this.symbol.toLowerCase()}`, 207, parseInt(Date.now() / 1000)]
      //   })
      // }
    },
    subscribe() {
      // if (this.interval < 60) {
      //   this.sendMessage({cmd: 'sub', args: [`candle.M${this.interval}.${this.symbol.toLowerCase()}`]})
      // } else if (this.interval >= 60) {
      //   this.sendMessage({cmd: 'sub', args: [`candle.H${this.interval / 60}.${this.symbol.toLowerCase()}`]})
      // } else {
      //   this.sendMessage({cmd: 'sub', args: [`candle.D1.${this.symbol.toLowerCase()}`]})
      // }
      this.sendMessage({
        event: "SUBSCRIBE_CANDLES",
        interval: 1,
        pair: 'BTC-USD',
        timestamp: Math.floor(new Date().getTime() / 1000),
        count: 1440
      })
    },
    onMessage(data) {
      if (data.data && data.data.length) {
        const list = []
        const ticker = `${this.symbol}-${this.interval}`
        data.data.forEach(function (element) {
          list.push({
            time: this.interval !== 'D' || this.interval !== '1D' ? element.timestamp * 1000 : element.timestamp,
            open: element.open,
            high: element.high,
            low: element.low,
            close: element.close,
            volume: element.value
          })
        }, this)
        this.cacheData[ticker] = list
        this.lastTime = list[list.length - 1].time
        //this.subscribe()
      }
      if (true) {
        this.datafeeds.barsUpdater.updateData()
        const ticker = `${this.symbol}-${this.interval}`
        const barsData = {
          time: data.timestamp * 1000,
          open: data.open,
          high: data.high,
          low: data.low,
          close: data.close,
          volume: data.value
        }
        if (barsData.time >= this.lastTime && this.cacheData[ticker] && this.cacheData[ticker].length) {
          this.cacheData[ticker][this.cacheData[ticker].length - 1] = barsData
        }
      }
    },
    getBars(symbolInfo, resolution, rangeStartDate, rangeEndDate, onLoadedCallback) {
      if (this.interval !== resolution) {
        this.unSubscribe(this.interval)
        this.interval = resolution
        // if (resolution < 60) {
        //   this.sendMessage({
        //     cmd: 'req',
        //     args: [`candle.M${this.interval}.${this.symbol.toLowerCase()}`, 1440, parseInt(Date.now() / 1000)]
        //   })
        // } else if (resolution >= 60) {
        //   this.sendMessage({
        //     cmd: 'req',
        //     args: [`candle.H${this.interval / 60}.${this.symbol.toLowerCase()}`, 1440, parseInt(Date.now() / 1000)]
        //   })
        // } else {
        //   this.sendMessage({
        //     cmd: 'req',
        //     args: [`candle.D1.${this.symbol.toLowerCase()}`, 800, parseInt(Date.now() / 1000)]
        //   })
        // }
        this.sendMessage({
          event: "SUBSCRIBE_CANDLES",
          interval: 1,
          pair: 'BTC-USD',
          timestamp: Math.floor(new Date().getTime() / 1000),
          count: 1440
        })

      }
      const ticker = `${this.symbol}-${this.interval}`
      if (this.cacheData[ticker] && this.cacheData[ticker].length) {
        this.isLoading = false
        const newBars = []
        this.cacheData[ticker].forEach(item => {
          if (item.time >= rangeStartDate * 1000 && item.time <= rangeEndDate * 1000) {
            newBars.push(item)
          }
        })
        onLoadedCallback(newBars)
      } else {
        const self = this
        this.getBarTimer = setTimeout(function () {
          self.getBars(symbolInfo, resolution, rangeStartDate, rangeEndDate, onLoadedCallback)
        }, 10)
      }
    }
  }
}