import jQuery from 'jquery'
import store from '../main/stores/index'
import moment from 'moment'
import _ from 'underscore'
import {Decimal} from 'decimal.js'


const submitFormWithRedirect = (url, body) => {
  let form = document.createElement('form');
  form.action = url;
  form.method = 'POST';
  Object.keys(body).forEach((name) => {
    let input = document.createElement('input');
    input.name = name;
    input.type = 'hidden';
    input.value = body[name];
    form.appendChild(input)
  });
  document.body.appendChild(form);
  form.submit();
};
const formLoader = (form, data) => {
  if (data === undefined) {
    return
  }

  for (const attribute in form) {
    if (data.hasOwnProperty(attribute)) {
      form[attribute] = data[attribute]
    }
  }
};
const checkFormChanges = (form, data) => {
  if (data === undefined) {
    return
  }

  for (let attribute in form) {
    if (data.hasOwnProperty(attribute)) {
      if (form[attribute] !== data[attribute]) {
        return false
      }
    }
  }
  return true
};

const hasAttribute = (model, attribute) => {
  return model === undefined
    || model.hasOwnProperty(attribute)
};

const handlerErrors = (elements, errors) => {

  if (elements !== null) {
    Object.keys(elements).map((key) => {
      if (/^error_[a-z]*$/.test(key) && elements[key] !== undefined) {
        elements[key].innerText = ''
      }
    })
  }

  const result = [];
  let errorAddedForKey = {};

  if (Array.isArray(errors)) {
    errors.forEach(error => {
      let params = (typeof error.params !== 'undefined' ? Object.assign({}, error.params) : {})
      let text = (Vue.i18n.keyExists("errors." + error.code) ? Vue.i18n.translate("errors." + error.code, params) : error.text)
      if (typeof error.field === 'undefined') {
        result.push(text)
      } else {
        if (text !== '' && typeof errorAddedForKey[error.field] === 'undefined' && elements !== null) {
          jQuery(elements['error_' + error.field]).text(text)
          jQuery(elements['field_' + error.field]).addClass('is-invalid')
          errorAddedForKey[error.field] = true
        }
      }
    })
  } else if (errors) {
    result.push(errors.message)
    console.log('ERROR: ' + errors.message)
  }

  return result
};

const handleErrorForAlert = (errors) => {
  let result = '';
  errors.forEach(error => {
    let params = (typeof error.params !== 'undefined' ? Object.assign({}, error.params) : {});
    let text = (Vue.i18n.keyExists("errors." + error.code) ? Vue.i18n.translate("errors." + error.code, params) : error.text);

    if (text !== '' && typeof text !== 'undefined') {
      result += text + '\n'
    }
  });
  if (result === '') {
    result = 'Operation failed.'
  }
  return result
};

const resetErrors = (elements) => {
  Object.keys(elements).map(key => {
    if (key.includes('error_')) {
      jQuery(elements[key]).text('')
    }
    if (key.includes('field_')) {
      jQuery(elements[key]).removeClass('is-invalid')
    }
  })
};

const formatMoney = (amount, currency, isString = true) => {

  if(!amount) return amount;
  let currencies, currencySetting, cryptoCurrencies, precision;
  amount = new Decimal(amount);
  currencies = store.getters.getCurrencies;
  cryptoCurrencies = store.getters.getCurrenciesByType.crypto_bill;

  if (typeof currencies === 'object') {
    currencySetting = currencies.find(setting => setting.name === currency);
  }
  if (currencySetting !== undefined) {
    precision = currencySetting.number_of_decimals
  } else {
    // When currencies not found in store use default method (crypto = 8 digits, fiat = 2 digits)
    precision = (typeof cryptoCurrencies === 'object' && cryptoCurrencies.includes(currency)) ? 8 : 2;
  }

  amount = amount.toFixed(precision, Decimal.ROUND_DOWN);

  if(isString) {
    amount = amount.replace(/(\.[0-9]*[1-9])0+$|\.0*$/,'$1')
  }

  return amount
};

const getCurrencySymbol = (currency) => {
  let symbols = {
    "BTC": "₿",
    "USD": "$",
    "EUR": "€",
    "RUB": "₽",
    "GBP": "£",
    "ETH": "Ξ",
    "USDT": "₮",
  };
  return symbols[currency]
};
const getCurrencyAlias = (currency) => {

  let symbol = _.find(store.getters.getCurrencies, {name: currency});

  if (symbol) {
    return symbol.view;
  }

  return currency
};

const formatDateTime = (date, format = 'DD.MM.YYYY, HH:mm:ss', lang = 'en') => {
  moment.locale(lang);
  return moment.parseZone(date).utc(false).format(format);
};

const formatDate = (date) => {
  return date ? moment.parseZone(date).utc(false).format('DD.MM.YYYY') : ''
};

const inputValidator = (e, filterName) => {
  let filters = {
    onlyNumbersFormat: /[^\d]+/g,
    floatFormat: /[^\d.]+/g,
    surname: /[^a-zа-я- ]+/gi,
    onlyLettersFormat: /[^a-zа-я]+/gi,
    onlyLettersAndSpacesFormat: /[^a-zа-я ]+/gmi,
    passportDepartmentCode: /[^0-9\-]/g,
    street: /[^a-zа-я0-9\-\/\s]+/gi,
    houseNumber: /[^a-zа-я0-9\/]+/gi,
    onlyLettersAndNumbersFormat: /[^a-zа-я0-9\/]+/gi,
    swiftFormat: /[^a-zA-Z\d]+/gm,
    recipientAccountFormat: /[^\d]+/g,
    addressFormat: /[^a-zа-я \d,\-/]+/gmi
  };

  e.target.value = e.target.value.replace(filters[filterName], '');
  let changeEvent = new Event('change');
  e.target.dispatchEvent(changeEvent);

};

const manualValidator = (value, filterName) => {

  let filters = {
    recipientAccountFormat: /[^\d]+/g,
    swiftFormat: /[^a-zA-Z\d]+/gm,
  };

  return value.replace(filters[filterName], '');
};

const copy = (str) => {
  let tmp = document.createElement('input'), // Создаём новый текстовой input
    focus = document.activeElement; // Получаем ссылку на элемент в фокусе (чтобы не терять фокус)
  tmp.value = str; // Временному input вставляем текст для копирования

  document.body.appendChild(tmp); // Вставляем input в DOM
  tmp.select(); // Выделяем весь текст в input
  document.execCommand('copy'); // Копирует в буфер выделенный текст
  document.body.removeChild(tmp); // Удаляем временный input
  focus.focus(); // Возвращаем фокус туда, где был
};

const getImgLogoPath = (currency) => {
  let logos = {
    BTC: '/assets/images/bc-logo.png',
    ETH: '/assets/images/ethereum.png',
    USDT: '/assets/images/tether.png',
  };
  return logos[currency]
};
const getCurrencyIcon = (currency) => {
  let currencies = store.getters.getCurrencies;
  let currentCurrency = _.find(currencies, {name: currency});
  return (currentCurrency && (currentCurrency.icon !== null)) ?
      `/uploads/${currentCurrency.icon}` : `/assets/images/currency/${currentCurrency.name}.png`
};
const userCardStatuses = {'CREATED': 'created', 'PROCESSING': 'on_verification', 'NOT_VERIFIED': 'canceled', 'VERIFIED': 'active'}
const userCardStatusLabel = (value) => {
  return Vue.i18n.translate(`statusCodes.cards.${userCardStatuses[value]}`);
};
const bankRequisiteStatuses = {'PROCESSING': 'on_verification', 'CANCELED': 'canceled', 'VERIFIED': 'active'}
const bankRequisiteStatus = (value) => {
  return Vue.i18n.translate(`statusCodes.bank_requisite.${bankRequisiteStatuses[value]}`);
};
const verifiedStatuses = {'VERIFIED': 'is_verified', 'PROCESSING': 'verification_in_progress', 'NOT_VERIFIED': 'is_not_verified', 'CREATED': 'is_created'}
const verifiedStatusTitle = (value) => {
  return Vue.i18n.translate(`statusCodes.profile.${verifiedStatuses[value]}`);
};
/**
 *
 * @param currency
 * @param pair
 * @returns boolean
 */
const checkUserAllowedCurrencies = ({currency = null, pair = null}) => {
  if (store.getters.isUserVerified) {
    return true
  } else if (currency && (store.getters.getCryptoCurrencies.includes(currency) || store.getters.getTokenCurrencies.includes(currency))) {
    return true
  } else {
    return !!(pair && pair.split('-').every((pC) => {
      return checkUserAllowedCurrencies({currency: pC})
    }))
  }
};

const formatInputTextFloat = (value, prevValue) => {
  if (value !== null) {
    value = value.toString().replace(',', '.');
    if (!(/^[0-9]*\.?[0-9]*$/g.test(value))) {
      value = prevValue
    }
  }
  return value
};

const sortBy = (array, field) => {
  return _.sortBy(array, field);
};

const filterBankRequisites = (currency, userBankRequisites, systemBankRequisites) => {
  let bankTypeWithCurrency = Object.keys(systemBankRequisites)
    .filter(bankType => systemBankRequisites[bankType][currency] !== undefined);
  return userBankRequisites
    .filter(bankRequisite => bankTypeWithCurrency.includes(bankRequisite.bank_type))
};

/**
 * Check on valid keyCode for phone number
 * good codes for 0-9, +, + with pressed shift key
 * arrows left, right, del, backspace, enter
 * @param shiftKey state of shiftKey
 * @param charCode pressed char code
 * @returns {boolean} if true then key is valid
 */
const keyCodeValidation = (shiftKey, charCode) => {
  return (!shiftKey && ((charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105))) ||
    [8, 13, 16, 35, 36, 37, 39, 43, 46, 107, 187].indexOf(charCode) !== -1;
};

/**
 * Check on mask length exceed. Valid charCodes when length == maxlength
 * backspace, delete, home, end, arrows (left, right)
 * @param charCode pressed char code
 * @param currentLength - current input length
 * @param maxLength - max input length
 * @returns {boolean} if key restricted and length maxed - prevent default
 */
const keyCodeValidationWhenFullLength = (charCode, currentLength, maxLength) => {
  return ([8, 35, 36, 37, 39, 46].indexOf(charCode) === -1) &&
    ((currentLength + 1) > maxLength);
};

/**
 * check pregmatch for phone number
 * @param str
 * @returns {RegExpMatchArray | Promise<Response | undefined> | *}
 */
const phoneValidation = (str) => {
  return str.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
};

const emailValidation = (str) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(str).toLowerCase());
};

const baseCurrency = (pair) => {
  return pair.split('-')[0]
};
const quoteCurrency = (pair) => {
  return pair.split('-')[1]
};
const reversePair = (pair) => {
  return pair.split('-').reverse().join('-')
};
const parseFixed = (amount) => {
  return parseFloat(amount).toFixed(5) * 1
};
const deviceTypeLabel = (value, item) => {
  value = value ? 'Mobile' : 'Desktop';
  if (item.os) {
    value += `, ${item.os} ${item.os_version}`
  }
  return value;
};
const getNameWallet = (currency) => {
  let symbols = {
    "BTC": "Bitcoin",
    "ETH": "Ethereum",
    "USDT": "Tether",
  };
  return symbols[currency]
};

const transactionIcon = (index) => {
  switch (index) {
    case 'PW':
    case 'CWCB':
      return '/assets/images/red-arrow-right.png';
    case 'CMP':
      return '/assets/images/red-arrow-right.png';
    case 'VPA':
      return '/assets/images/red-arrow-right.png';
    case 'FVPA':
      return '/assets/images/red-arrow-right.png';
    case 'INV':
      return '/assets/images/red-arrow-right.png';
    case 'FINV':
      return '/assets/images/red-arrow-right.png';
    case 'EXF':
      return '/assets/images/red-arrow-right.png';
    case 'IB':
      return '/assets/images/left-arrow-right.png';
    case 'EXT':
      return '/assets/images/left-arrow-right.png';
    case 'СME':
      return '/assets/images/red-arrow-right.png';
    case 'СMEM':
    case 'СMET':
      return '/assets/images/left-arrow-right.png';
    case 'CTW':
      return '/assets/images/green-left-arrow.png';
    case 'CTWC':
      return '/assets/images/green-left-arrow.png';
    case 'DP':
      return '/assets/images/green-left-arrow.png';
    case 'CEXF':
      return '/assets/images/green-left-arrow.png';
    case 'AV':
      return '/assets/images/green-left-arrow.png';
    case 'СVPA':
      return '/assets/images/green-left-arrow.png';
    case 'СFVPA':
      return '/assets/images/green-left-arrow.png';
    case 'EXTB':
      return '/assets/images/green-left-arrow.png';
    case 'TEXT':
      return '/assets/images/green-left-arrow.png';
    case 'TСME':
      return '/assets/images/red-arrow-right.png';
    case 'TEXF':
      return '/assets/images/red-arrow-right.png';
    case 'BCA':
      return '/assets/images/green-left-arrow.png';
    case 'EXI':
      return '/assets/images/red-arrow-right.png';
    case 'СINV':
      return '/assets/images/green-left-arrow.png';
    case 'PINVP':
      return '/assets/images/green-left-arrow.png';
    case 'PINV':
      return '/assets/images/green-left-arrow.png';
    case 'EXIB':
      return '/assets/images/green-left-arrow.png';
    case 'FAD':
      return '/assets/images/green-left-arrow.png';
    case 'IBI':
      return '/assets/images/green-left-arrow.png';
    case 'FIX':
      return '/assets/images/red-arrow-right.png';
    case 'ED':
      return '/assets/images/green-left-arrow.png';
    case 'СED':
      return '/assets/images/red-arrow-right.png';
    case 'BD':
      return '/assets/images/green-left-arrow.png';
    case 'PD':
      return '/assets/images/green-left-arrow.png';
  }
};
const transactionSymbol = (index) => {
  switch (index) {
    case 'PW':
    case 'CWCB':
      return '-';
    case 'CMP':
      return '-';
    case 'EXF':
      return '-';
    case 'СME':
      return '-';
    case 'СMEM':
    case 'СMET':
    case 'VPA':
      return '-';
    case 'IB':
      return '+';
    case 'FVPA':
      return '-';
    case 'INV':
      return '-';
    case 'FINV':
      return '-';
    case 'EXT':
      return '+';
    case 'CTW':
      return '+';
    case 'CTWC':
      return '+';
    case 'DP':
      return '+';
    case 'CEXF':
      return '+';
    case 'СVPA':
      return '+';
    case 'СFVPA':
      return '+';
    case 'AV':
      return '+';
    case 'EXTB':
      return '+';
    case 'TEXT':
      return '+';
    case 'TСME':
      return '-';
    case 'TEXF':
      return '-';
    case 'BCA':
      return '+';
    case 'EXI':
      return '-';
    case 'СINV':
      return '+';
    case 'PINVP':
      return '+';
    case 'PINV':
      return '+';
    case 'EXIB':
      return '+';
    case 'FAD':
      return '+';
    case 'IBI':
      return '+';
    case 'FIX':
      return '-';
    case 'ED':
      return '+';
    case 'СED':
      return '-';
    case 'BD':
      return '+';
    case 'PD':
      return '+';
  }
};

let transactionsWithReplacement = [
  'PWA', 'OOTPA', 'OTPA', 'IB', 'PINVP', 'PINV', 'FAD', 'FIX', 'BD', 'PD',
];
const transactionTitle = (transaction) => {
  let key = `transactions.comments.${transaction.index}`
  if (transactionsWithReplacement.includes(transaction.index)) {
    return transaction.comment.replace(Vue.i18n.translateIn('EN', key), Vue.i18n.translate(key))
  }
  let translate = null
  if (transaction.withdrawals_count > 0) {
    key += `_${transaction.withdrawals.payment_type}`
    translate = Vue.i18n.translate(key, [transaction.withdrawals.currency_from, transaction.withdrawals.currency_to])
  } else if (transaction.replenishments_count > 0) {
    key += `_${transaction.deposit.payment_type}`
    translate = Vue.i18n.translate(key, [transaction.replenishments.currency_from, transaction.replenishments.currency_to])
  }
  if ((translate === null) || (translate === key)) {
    translate = Vue.i18n.translate(`transactions.comments.${transaction.index}`)
  }
  return translate
}

const getLangFlagIcon = (lang) => {
  switch (lang) {
    case 'EN':
      return '/assets/images/flag-en.png';
    case 'RU':
      return '/assets/images/flag-ru.png'
  }
};

const getBlockchainExplorerUrl = (currency) => {
  if (currency === 'BTC') {
    return 'https://www.blockchain.com/en/btc/tx/'
  } else {
    return 'https://etherscan.io/tx/'
  }
};

const isMobile = () =>{
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
};

export {
  isMobile,
  getLangFlagIcon,
  getCurrencySymbol,
  formLoader,
  hasAttribute,
  handlerErrors,
  resetErrors,
  formatMoney,
  formatDateTime,
  formatDate,
  getCurrencyAlias,
  inputValidator,
  manualValidator,
  checkFormChanges,
  copy,
  getImgLogoPath,
  getCurrencyIcon,
  checkUserAllowedCurrencies,
  formatInputTextFloat,
  sortBy,
  filterBankRequisites,
  keyCodeValidation,
  keyCodeValidationWhenFullLength,
  phoneValidation,
  emailValidation,
  baseCurrency,
  quoteCurrency,
  reversePair,
  userCardStatusLabel,
  bankRequisiteStatus,
  verifiedStatusTitle,
  parseFixed,
  deviceTypeLabel,
  submitFormWithRedirect,
  getNameWallet,
  transactionIcon,
  transactionSymbol,
  transactionTitle,
  handleErrorForAlert,
  getBlockchainExplorerUrl
}

