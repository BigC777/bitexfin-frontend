export default {
  state: {
    active_socket: false,
    exchange_rates: null,
    exchange_accounts: null,
    exchange_rates_header: [],
  },
  getters: {
    getActiveSocket: state => state.active_socket,
    getExchangeAccounts: state => state.exchange_accounts,
    getExchangeRates: state => state.exchange_rates,
    getExchangeRatesHeader: state => state.exchange_rates_header,
  },
  mutations: {
    updateExchangeAccounts(state, exchangeAccounts) {
      state.exchange_accounts = exchangeAccounts;
      state.exchange_rates_header = [];

      let main_currencies = [
        'BTC-USD',
        'BTC-EUR',
        'BTC-GBP',
        'ETH-USD',
      ];

      Object.keys(exchangeAccounts).map(rate => {
        let pair = rate.currency_from + '-' + rate.currency_to;
        if (main_currencies.includes(pair)) {
          state.exchange_rates_header.push(rate);
        }
      })

    },
    updateExchangeRates(state, exchangeRates) {
      state.exchange_rates = exchangeRates;
      state.exchange_rates_header = [];

      let main_currencies = [
        'BTC-USD',
        'BTC-EUR',
        'BTC-GBP',
        'ETH-USD',
      ];
      Object.keys(exchangeRates).map(pair => {
        if (main_currencies.includes(pair)) {
          state.exchange_rates_header.push(exchangeRates[pair][0]);
        }
      })
    },
    mutate(state, {type, items}) {
      state[type] = items;
    }
  },
  actions: {
    loadExchangeRates({commit, getters}) {
      const url = getters.getSocketUrl;
      let ws = new WebSocket(url);

      ws.onopen = function () {
        commit('mutate', {type: 'active_socket', items: true});
        console.log('websocket is connected ...');
        ws.send('raw_rates');
      };
      ws.onmessage = function (message) {
        commit('updateExchangeRates', JSON.parse(message.data))
      };
      ws.onerror = function () {
        commit('mutate', {type: 'active_socket', items: false});
        ws = new WebSocket(url);
      };
      ws.onclose = function () {
        commit('mutate', {type: 'active_socket', items: false});
        ws = new WebSocket(url);
      };
    },
  },
}
