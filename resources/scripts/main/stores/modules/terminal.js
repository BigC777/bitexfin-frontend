import requests from '../../requests'
import Vue from 'vue'

export default {
  state: {
    orderPairAllowedForUnverified: true,
    orderPair: 'ETH-BTC',
    orderType: 'MARKET',
    orderSide: 'SELL',
    orderSystem: 'INTERNAL',
    publicOrders: {'asks': [], 'bids': []},
    exchangePairs: [],
    publicTrades: null,
    userBalances: null,
    userTrades: null,
    userActiveOrders: null,
    userFilledOrders: null,
    isActiveTerminalSocket: false,
    terminalWebsocket: null,
    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    }
  },
  getters: {
    getSocket: state => state.socket,
    getOrderSide: state => state.orderSide,
    getOrderType: state => state.orderType,
    getOrderPair: state => state.orderPair,
    getExchangePairs: state => state.exchangePairs,
    getPublicOrders: state => state.publicOrders,
    getPublicTrades: state => state.publicTrades,
    getUserBalances: state => state.userBalances,
    getUserTrades: state => state.userTrades,
    getUserActiveOrders: state => state.userActiveOrders,
    getUserFilledOrders: state => state.userFilledOrders,
    getIsActiveTerminalSocket: state => state.isActiveTerminalSocket,
    getOrderPairAllowedForUnverified: state => state.orderPairAllowedForUnverified
  },
  mutations: {
    setExchangePairs(state, data) {
      state.exchangePairs = data.exchange_pairs
    },
    updateOrderPairAllowedForUnverified(state, obj) {
      let currencies = obj.pair.split('-')
      state.orderPairAllowedForUnverified =
        obj.cryptoCurrencies.includes(currencies[0]) &&
        obj.cryptoCurrencies.includes(currencies[1])
    },
    updateOrderPair(state, pair) {
      state.orderPair = pair
    },
    updateOrderType(state, orderType) {
      state.orderType = orderType
    },
    updateOrderSide(state, orderSide) {
      state.orderSide = orderSide
    },
    resetTerminal(state) {
      state.userTrades = null
      state.publicTrades = null
      state.publicOrders = null
      state.userActiveOrders = null
    },
    mutate(state, {type, items}) {
      state[type] = items;
    },
    connectToTerminalWs(state, {url, token}) {

    },
    updateTerminal(state, data) {
      let balances = data['balances']
      let userTrades = data['trades']
      let publicTrades = data['public_trades']
      let publicOrders = data['public_orders']
      let activeOrders = data['active_orders']
      if (balances !== undefined) {
        let newBalances = {}
        balances.forEach(balance => {
          newBalances[balance.currency] = balance.amount
        })
        state.userBalances = newBalances
      }
      if (userTrades !== undefined) {
        state.userTrades = userTrades.length > 0 ? userTrades : null
      }
      if (publicTrades !== undefined) {
        state.publicTrades = publicTrades.length > 0 ? publicTrades : null
      }
      if (publicOrders !== undefined) {
        state.publicOrders = publicOrders
      }
      if (activeOrders !== undefined) {
        state.userActiveOrders = activeOrders.length > 0 ? activeOrders : null
      }
    },
  },
  actions: {
    loadExchangePairs({commit}) {
      return new Promise((resolve, reject) => {
        requests.terminal.getExchangePairs().then(({data}) => {
          commit('setExchangePairs', data)
          resolve(data)
        }).catch(({response}) => {
          reject(response)
        })
      })
    },
    restartTerminalSubscription({dispatch}) {
      Vue.prototype.$socket = undefined
      Vue.prototype.$disconnect()
      dispatch('subscribeToTerminalWs')
    },
    setIntervalForResubscribe({dispatch}) {
      setTimeout(() => dispatch('restartTerminalSubscription'), 3 * 60000)
    },
    subscribeToTerminalWs({commit, getters, dispatch}) {
      const url = getters.getHistoryApiWsUrl
      const token = getters.getUserToken
      const pair = getters.getOrderPair
      const subscription = {event: 'SUBSCRIBE', pair: pair}

      if (Vue.prototype.$socket === undefined || Vue.prototype.$socket.readyState !== 1) {
        Vue.prototype.$connect(url,
          {
            protocol: token,
            connectManually: true,
            reconnection: true,
            reconnectionDelay: 3000,
          })
        Vue.prototype.$socket.onopen = function () {
          console.log('terminal is connected...')
          if (Vue.prototype.$socket.readyState === 1) {
            Vue.prototype.$socket.send(JSON.stringify(subscription))
            Vue.prototype.$socket.onmessage = function (message) {
              commit('updateTerminal', JSON.parse(message.data))
            };
          }
        }
      } else {
        commit('resetTerminal')
        Vue.prototype.$socket.send(JSON.stringify(subscription))
        Vue.prototype.$socket.onmessage = function (message) {
          commit('updateTerminal', JSON.parse(message.data))
        };
      }
      dispatch('setIntervalForResubscribe')
    },
    setOrderType({commit}, orderType) {
      commit('updateOrderType', orderType)
    },
    setOrderSide({commit}, orderSide) {
      commit('updateOrderSide', orderSide)
    },
    setOrderPair({commit, getters}, pair) {
      commit('updateOrderPair', pair)
      commit('updateOrderPairAllowedForUnverified',
        {
          pair: pair,
          cryptoCurrencies: getters.getCryptoCurrencies
        })
    }
  },
}
