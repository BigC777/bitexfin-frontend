import requests from '../../requests'

export default {
  state: {
    token: localStorage.getItem('token') || null,
    access: false,
    data: {},
    verified: false,
    notVerified: false,
    onVerify: false,
    created: false,
    loaded: false,
    userVouchers: undefined,
    selectedLang: localStorage.getItem('lang') || 'EN',
    legalEntity: 0,
    openRegisterModal: false,
    openLoginModal: false
  },
  getters: {
    accessCheck: state => !!state.access,
    isAuthenticated: state => !!state.token,
    isUserVerified: state => state.verified,
    isUserNotVerified: state => state.notVerified,
    isUserOnVerify: state => state.onVerify,
    isUserCreated: state => state.created,
    getUser: state => state.data,
    getUserAuthHistory: state => state.data.auth_history,
    isUserLoaded: state => state.loaded,
    getUserVouchers: state => state.userVouchers,
    getUserToken: state => state.token,
    getSelectedLang: state => state.selectedLang,
    getLegalEntity: state => state.legalEntity,
    getOpenRegisterModal: state => state.openRegisterModal,
    getOpenLoginModal: state => state.openLoginModal,

  },
  mutations: {
    login(state, token) {
      state.token = token
    },
    accessSet(state, access) {
      state.access = access
    },
    updateUser(state, user) {
      state.data = user;
      state.userVouchers = user.vouchers_from.concat(user.vouchers_to)
      state.verified = user.verified === 'VERIFIED';
      state.onVerify = user.verified === 'PROCESSING';
      state.created = user.verified === 'CREATED';
      state.notVerified = user.verified === 'NOT_VERIFIED';
      state.loaded = true;
    },
    logout(state) {
      localStorage.removeItem('token')
      state.token = null
    },
    pushUserAuthHistory(state, authHistory) {
      state.data.auth_history = [...state.data.auth_history, ...authHistory]
    },
    pushUserAllowedIps(state, allowedIp) {
      if (state.data.allowed_ips) {
        state.data.allowed_ips.push(allowedIp);
      }
    },
    removeUserAllowedIp(state, index) {
      if (state.data.allowed_ips) {
        state.data.allowed_ips.splice(index, 1);
      }
    },
    updateUserLanguage(state, language) {
      state.selectedLang = language
      Vue.i18n.set(language)
      localStorage.setItem('lang', language);
      if (state.token !== null) {
        requests.dashboard.profile.postChangeLang({user_lang: language}).catch(e => {
        })
      }
    },
    updateLegalEntity(state, legal_entity) {
      state.legalEntity = legal_entity
    },
    setOpenRegisterModal(state, val) {
      state.openRegisterModal = val
    },
    setOpenLoginModal(state, val) {
      state.openLoginModal = val
    },
  },
  actions: {
    login({commit, dispatch}, {user, getCode}) {
      return new Promise((resolve, reject) => {
        requests.authenticate.login(user, getCode).then(({data}) => {
          if (data.isSendCode !== true /*&& data.unconfirmed_email !== true*/) {
            localStorage.setItem('token', data.token)
            commit('login', data.token)
          }
          resolve(data)
        }).catch(({response}) => {
          reject(response)
        })
      })
    },
    access({commit, dispatch}, redirect) {
      requests.check.access().then(({data}) => {
        if(redirect) document.location.href = '/dashboard'
      }).catch(({response}) => {
        commit('logout')
      })
    },
    logout({commit}) {
      return new Promise((resolve, reject) => {
        requests.authenticate.logout().then(({data}) => {
          commit('logout')
          resolve(data)
        }).catch(({response}) => {
          reject(response)
        })
      })
    },
    pushUserAuthHistory({commit}, authHistory) {
      commit('pushUserAuthHistory', authHistory);
    },
    updateUserLang({commit}, lang) {
      commit('updateUserLanguage', lang)
    },
    updateLegalEntity({commit}, legalEntity) {
      commit('updateLegalEntity', legalEntity)
    }
  },
}
