export default {
  state: {
    debug: false,
    appUrl: null,
    socketUrl: null,
    orderApiUrl: null,
    historyApiUrl: null,
    historyApiWsUrl: null,
    maintenanceStatus: false
  },
  getters: {
    getAppUrl: state => state.appUrl,
    getSocketUrl: state => state.socketUrl,
    getOrderApiUrl: state => state.orderApiUrl,
    getHistoryApiUrl: state => state.historyApiUrl,
    getHistoryApiWsUrl: state => state.historyApiWsUrl,
    getMaintenanceStatus: state => state.maintenanceStatus
  },
  mutations: {
    setAttribute(state, {attribute, value}) {
      if (state.hasOwnProperty(attribute)) {
        state[attribute] = value;
      }
    },
    changeMaintenanceStatus(state, status){
      state.maintenanceStatus = status
    }
  },
  actions:{
    setMaintenanceStatus({commit},status){
      commit('changeMaintenanceStatus',status)
    }
  }
};
