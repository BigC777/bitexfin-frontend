import requests from "../../requests";
import RoundAmount from "../../classes/RoundAmount";
import store from "../index";


const isEUCitizen = (country_iso) => {
  return ['AL', 'AD', 'AT', 'BE', 'BA', 'BG', 'HR', 'CY', 'CZ', 'DK',
    'EE', 'FO', 'FI', 'AX', 'FR', 'GE', 'DE', 'GI', 'GR', 'VA', 'HU', 'IS', 'IE', 'IT', 'LV',
    'LI', 'LT', 'LU', 'MT', 'MC', 'ME', 'NL', 'NO', 'PL', 'PT', 'RO', 'SM', 'RS', 'SK', 'SI',
    'ES', 'SJ', 'SE', 'CH', 'MK', 'GB', 'GG', 'JE', 'IM'].indexOf(country_iso)!==-1;
}

export default {
  state: {
    active_account: {
      type: Object,
      default() {
      }
    },
    user_accounts: [],
    user_limits: [],
    payment_types: [],
    currencies_by_types: [],
    currencies: [],
    user_cards: [],
    unverified_cards: [],
    show_creating_new_card: false,
    bank_refill_invoice_id: "",
    callback_url: "",
    country_codes: [],
    account_currency_for_payment: "ETH",
    payment_type_for_payment: 'crypto_bill',
    amount_for_payment: 0,
    amount_total: 0,
    amountForPaymentMinusCommission: 0,
    commission_for_payment: 0,
    user_transactions: [],
    user_transactions_total: 0,
    user_transactions_per_page: 0,
    bank_requisites: [],
    paysend_requisites: [],
    unverified_bank_requisites: [],
    canceled_bank_requisites: [],
    bank_types: [],
    verified_bank_requisites: [],
    crypto_requisites: [],
    user_unread_notifications: 0,
    system_bank_requisites: [],
    system_bank_for_payment: null,
    referral_link: '',
    commission_multiplier_for_payment: 0,
    commission_fix_for_payment: 0,
    isFillProfile: true,
    fillMessage: '',
    cryptoCurrencies: [],
    tokenCurrencies: [],
    fiatCurrencies: [],
    currenciesWithSortByTypeAndAlphabet: [],
    cryptoCurrencyObjects: [],
    tokenCurrencyObjects: [],
    fiatCurrencyObjects: [],
    availableWithdrawalCryptoCurrenciesForAddition: ['BTC', 'ETH', 'USDT'],
    currenciesForUnverifiedUsers: ['BTC', 'ETH', 'USDT'],
    bankRequisiteForPayment: null,
    cardForPayment: null,
    cryptoWalletForPayment: null,
    selectedCheckboxModal: false,
    client_ip: null,
    currenciesByBankType: {'SWIFT': [], 'SEPA': []},
    fillingPercentage: 0,
    fillingContacts: false,
    fillingPersonalData: false,
    fillingResidentialAddress: false,
    dashboard_load_status: false,
    voucherSettings: []
  },
  getters: {
    getSelectedCheckboxModal: state => state.selectedCheckboxModal,
    getAvailableWithdrawalCryptoCurrenciesForAddition: state => state.availableWithdrawalCryptoCurrenciesForAddition,
    getActiveAccount: state => state.active_account,
    getUserAccounts: state => state.user_accounts,
    getUserLimits: state => state.user_limits,
    getPaymentTypes: state => state.payment_types,
    getCurrenciesByType: state => state.currencies_by_types,
    getCurrencies: state => state.currencies,
    getAmountForPayment: state => state.amount_for_payment,
    getAmountTotalPayment: state => state.amount_total,
    getAccountCurrencyForPayment: state => state.account_currency_for_payment,
    getPaymentTypeForPayment: state => state.payment_type_for_payment,
    getCommissionForPayment: state => state.commission_for_payment,
    getBankRequisiteForPayment: state => state.bankRequisiteForPayment,
    getCardForPayment: state => state.cardForPayment,
    getCryptoWalletForPayment: state => state.cryptoWalletForPayment,
    getUserCards: state => state.user_cards,
    getUnverifiedCards: state => state.unverified_cards,
    getShowCreatingNewCard: state => state.show_creating_new_card,
    getBankRefillInvoiceID: state => state.bank_refill_invoice_id,
    getCallbackUrl: state => state.callback_url,
    getCountryCodes: state => state.country_codes,
    getUserTransactions: state => state.user_transactions,
    getUserTransactionsTotal: state => state.user_transactions_total,
    getUserTransactionsPerPage: state => state.user_transactions_per_page,
    getBankRequisites: state => state.bank_requisites,
    getPaysendRequisites: state => state.paysend_requisites,
    getUnverifiedBankRequisites: state => state.unverified_bank_requisites,
    getCanceledBankRequisites: state => state.canceled_bank_requisites,
    getBankTypes: state => state.bank_types,
    getCryptoRequisites: state => state.crypto_requisites,
    getCountUnreadUserNotifications: state => state.user_unread_notifications,
    getSystemBankRequisites: state => state.system_bank_requisites,
    getSystemBankForPayment: state => state.system_bank_for_payment,
    getReferralLink: state => state.referral_link,
    getCommissionMultiplierForPayment: state => state.commission_multiplier_for_payment,
    getFillProfile: state => state.isFillProfile,
    getFillMessage: state => state.fillMessage,
    getCryptoCurrencies: state => state.cryptoCurrencies,
    getTokenCurrencies: state => state.tokenCurrencies,
    getFiatCurrencies: state => state.fiatCurrencies,
    getCurrenciesWithSortByTypeAndAlphabet: state => state.currenciesWithSortByTypeAndAlphabet,
    getCommissionFixForPayment: state => state.commission_fix_for_payment,
    getCurrentIp: state => state.client_ip,
    getAmountForPaymentMinusCommission: state => state.amountForPaymentMinusCommission,
    getCurrenciesByBankType: state => state.currenciesByBankType,
    getCurrenciesForUnverifiedUsers: state => state.currenciesForUnverifiedUsers,
    getFillingPercentage: state => state.fillingPercentage,
    isFillingContacts: state => state.fillingContacts,
    isFillingPersonalData: state => state.fillingPersonalData,
    isFillingResidentialAddress: state => state.fillingResidentialAddress,
    GetVoucherSettings: state => state.voucherSettings,
    GetDashboardLoadStatus: state => state.dashboard_load_status,
    getCurrenciesWithoutFiat: state => {
      return state.user_accounts.filter(item => {
        return !state.fiatCurrencies.includes(item.currency)
      })
    }
  },
  mutations: {
    mutate(state, {type, items}) {
      state[type] = items;
    },
    pushUserTransactions(state, transactions) {
      state.user_transactions = [...state.user_transactions, ...transactions]
    },
    commissionForPayment(state, {type, direction, payment_type}) {
      if (payment_type === 'token_bill') payment_type = 'crypto_bill';
      let limits = state.user_limits;
      let currency = state.account_currency_for_payment;
      let amount = state.amount_for_payment;
      let limit;
      if (['deposit', 'withdrawal'].indexOf(direction) !== false) {

        let use_eu_comm = false;
        if (payment_type === 'online_translation' && isEUCitizen(state.cardForPayment.country_code)) {
          use_eu_comm = true;
        }

        if (limits[payment_type][currency]) {
          limit = limits[payment_type][currency][0];
          if (payment_type === 'rated_bill') {
            limits[payment_type][currency].forEach(bankLimit => {
              limit = bankLimit.bank_type === state.bankRequisiteForPayment.bank_type ? bankLimit : limit
            })
          }
          let number_of_decimals = store.getters.getCurrencies.find(setting => setting.name === currency).number_of_decimals;

          let commission_multiplier = parseFloat(((direction === 'deposit') ?
            (use_eu_comm && limit.eu_refill_commission_multiplier?limit.eu_refill_commission_multiplier : limit.refill_commission_multiplier) :
            (use_eu_comm && limit.eu_withdrawal_commission_multiplier?limit.eu_withdrawal_commission_multiplier : limit.withdrawal_commission_multiplier)));

          let commission_fix = parseFloat((direction === 'deposit') ?
            limit.refill_commission_fix : limit.withdrawal_commission_fix);

          state[type] = RoundAmount.ceil10((amount * commission_multiplier) + commission_fix, -number_of_decimals);
          state.amountForPaymentMinusCommission = amount - state[type].toFixed(number_of_decimals);
          state.commission_multiplier_for_payment = RoundAmount.floor10((commission_multiplier * 100).toFixed(number_of_decimals), -number_of_decimals);
          state.commission_fix_for_payment = commission_fix;
        }
      }
    },
    updateNotificationCounterMutation(state, {type}) {
      state[type] -= 1;
    },
    removeCard(state, {type, index}) {
      if (state[type]) {
        state[type].splice(index, 1);
      }
    },
    updateFillProfile(state, isFillProfile) {
      state.isFillProfile = isFillProfile
    },
    updateActiveAccount(state, {type, currency}) {
      state.user_accounts.map(user_account => {
        if (user_account.currency === currency) {
          state[type] = user_account
        }
      })
    },
    updateAvailableWithdrawalCryptoCurrenciesForAddition(state, cryptoRequisites) {
      if (cryptoRequisites['WITHDRAWAL']) {
        return {...Object.keys(cryptoRequisites['WITHDRAWAL'])};
      }
    },
    updateCryptoRequisite(state, cryptoRequisite) {
      if (!state.crypto_requisites[cryptoRequisite.type]) {
        state.crypto_requisites[cryptoRequisite.type] = {}
      }

      if (!state.crypto_requisites[cryptoRequisite.type][cryptoRequisite.currency]) {
        state.crypto_requisites[cryptoRequisite.type][cryptoRequisite.currency] = []
      }

      state.crypto_requisites[cryptoRequisite.type][cryptoRequisite.currency].push(cryptoRequisite)
    },
    updateCurrenciesByBankType(state, systemBankRequisites) {
      for (let bankType in systemBankRequisites) {
        state.currenciesByBankType[bankType] = Object.keys(systemBankRequisites[bankType])
      }
    },
    setCurrencies(state, currencies) {
      state.currencies = currencies
      for (let key in currencies) {
        if (currencies[key].type === 'FIAT') {
          state.fiatCurrencyObjects.push(currencies[key])
          state.fiatCurrencies.push(currencies[key].name)
        } else if (currencies[key].type === 'CRYPTO') {
          state.cryptoCurrencyObjects.push(currencies[key])
          state.cryptoCurrencies.push(currencies[key].name)
        } else if (currencies[key].type === 'TOKEN') {
          state.tokenCurrencyObjects.push(currencies[key])
          state.tokenCurrencies.push(currencies[key].name)
        }
      }
      state.currenciesWithSortByTypeAndAlphabet = state.fiatCurrencies.concat(state.cryptoCurrencies.concat(state.tokenCurrencies))
    },
    resetUserTransactions(state) {
      state.user_transactions = state.user_transactions.slice(0, 20)
    },
  },
  actions: {
    loadDashboard({commit}) {
      commit('mutate', {type: 'dashboard_load_status', items: true});
      return requests.dashboard.getIndexData().then(({data}) => {

        commit('updateUser', data.user);
        commit('updateUserLanguage', data.user.language);
        if (data.unread_notifications !== undefined) {
          commit('mutate', {
            items: data.unread_notifications || 0,
            type: 'user_unread_notifications'
          });
        }
        commit('mutate', {type: 'user_accounts', items: data.user_accounts});
        commit('mutate', {type: 'user_cards', items: data.user_cards});
        commit('mutate', {type: 'unverified_cards', items: data.unverified_cards});
        commit('mutate', {type: 'user_limits', items: data.limits});
        commit('mutate', {type: 'payment_types', items: data.payment_types});
        commit('mutate', {type: 'callback_url', items: data.callback_url});
        commit('mutate', {type: 'referral_link', items: data.user_referral_link});
        commit('updateFillProfile', data.is_fill_profile);
        commit('mutate', {type: 'fillMessage', items: data.fill_message});
        commit('mutate', {type: 'client_ip', items: data.client_ip});
        commit('mutate', {type: 'fillingPercentage', items: data.filling_percentage});
        commit('mutate', {type: 'fillingContacts', items: data.filling_contacts});
        commit('mutate', {type: 'fillingPersonalData', items: data.filling_personal_data});
        commit('mutate', {type: 'fillingResidentialAddress', items: data.filling_residential_address});

        let ccs = data.country_codes;
        let country_codes = Object.keys(ccs).map(key => {
          return {code: key, title: ccs[key]}
        });

        commit('mutate', {type: 'country_codes', items: country_codes});
        commit('mutate', {type: 'user_transactions', items: data.user_transactions});
        commit('mutate', {type: 'active_account', items: data.user_accounts[0]});
        commit('mutate', {type: 'bank_requisites', items: data.bank_requisites});
        commit('mutate', {type: 'paysend_requisites', items: data.paysend_requisites});
        commit('mutate', {type: 'unverified_bank_requisites', items: data.unverified_bank_requisites});
        commit('mutate', {type: 'canceled_bank_requisites', items: data.canceled_bank_requisites});
        commit('mutate', {type: 'system_bank_requisites', items: data.system_bank_requisites});
        commit('updateCurrenciesByBankType', data.system_bank_requisites)
        commit('mutate', {type: 'bank_types', items: data.bank_types});
        commit('mutate', {type: 'crypto_requisites', items: data.crypto_requisites});
        commit('mutate', {type: 'crypto_requisites_withdrawal', items: data.crypto_requisites_withdrawal});
        commit('updateAvailableWithdrawalCryptoCurrenciesForAddition', data.crypto_requisites);
        commit('mutate', {
          items: data.user_accounts[0] ? data.user_accounts[0].currency : '',
          type: 'account_currency_for_payment'
        });
        commit('mutate', {type: 'voucherSettings', items: data.voucher_settings})
      });
    },
    updateDashboard({commit}) {
      return requests.dashboard.getIndexData().then(({data}) => {
        commit('updateUser', data.user);
        commit('mutate', {type: 'user_accounts', items: data.user_accounts});
        commit('mutate', {type: 'user_limits', items: data.limits});
        commit('mutate', {type: 'payment_types', items: data.payment_types});
        commit('mutate', {type: 'user_cards', items: data.user_cards});
        commit('mutate', {type: 'unverified_cards', items: data.unverified_cards});
        commit('mutate', {type: 'user_transactions', items: data.user_transactions});
      });
    },
    updateUserAccount({commit}, currency = null) {
      return new Promise((resolve, reject) => {
        requests.dashboard.getTransactions(currency).then(({data}) => {
          commit('mutate', {type: 'user_accounts', items: data.user_accounts});
          commit('pushUserTransactions', data.user_transactions_all);
          commit('mutate', {type: 'user_transactions_total', items: data.total});
          commit('mutate', {type: 'user_transactions_per_page', items: data.per_page});
          resolve()
        });
      }).catch(error=>{
        reject()
      })
    },
    userAgree({commit}) {
      return requests.dashboard.userAgree()
    },
    loadAllTransactions({commit}, page = 0) {
      return new Promise((resolve, reject) => {
        requests.dashboard.getUserTransactionsAll(page).then(({data}) => {
          commit('pushUserTransactions', data.user_transactions_all);
          commit('mutate', {type: 'user_transactions_total', items: data.total});
          commit('mutate', {type: 'user_transactions_per_page', items: data.per_page});
          resolve()
        }).catch(error=>{
          reject()
        })
      })

    },
    resetPreviewTransactions({commit}) {
      commit('resetUserTransactions');
    },
    setCurrenciesByTypes({commit}, currenciesByTypes) {
      commit('mutate', {type: 'currencies_by_types', items: currenciesByTypes});
    },
    setCurrencies({commit}, currencies) {
      commit('setCurrencies', currencies);
    },
    setActiveAccount({commit}, account) {
      commit('mutate', {type: 'active_account', items: account});
      commit('mutate', {type: 'account_currency_for_payment', items: account.currency});
    },
    setAmountForPayment({commit}, amount) {
      commit('mutate', {type: 'amount_for_payment', items: amount});
    },
    setAmountTotalPayment({commit}, amount) {
      commit('mutate', {type: 'amount_total', items: amount});
    },
    setAccountCurrencyForPayment({commit}, account_currency) {
      commit('mutate', {type: 'account_currency_for_payment', items: account_currency});
      commit('updateActiveAccount', {type: 'active_account', currency: account_currency});
    },
    setPaymentTypeForPayment({commit}, payment_type) {
      commit('mutate', {type: 'payment_type_for_payment', items: payment_type});
    },
    updateCommissionForPayment({commit}, items) {
      commit('commissionForPayment', {type: 'commission_for_payment', direction: items[0], payment_type: items[1]})
    },
    updateAvailableWithdrawalCryptoCurrenciesForAddition({commit}, currency) {
      commit('mutate', {type: 'updateAvailableWithdrawalCryptoCurrenciesForAddition', items: currency})
    },
    setBankRefillInvoiceID({commit}, invoice_id) {
      commit('mutate', {type: 'bank_refill_invoice_id', items: invoice_id});
    },
    removeNotification({commit}) {
      commit('updateNotificationCounterMutation', {type: 'user_unread_notifications'});
    },
    setSystemBankForPayment({commit}, system_bank) {
      commit('mutate', {type: 'system_bank_for_payment', items: system_bank});
    },
    setUserAccounts({commit}, accounts) {
      commit('mutate', {type: 'user_accounts', items: accounts});
    },
    setUserTransactions({commit}, accounts) {
      commit('mutate', {type: 'user_transactions', items: accounts});
    },
    setBankRequisiteForPayment({commit}, bankRequisite) {
      commit('mutate', {type: 'bankRequisiteForPayment', items: bankRequisite});
    },
    setCardForPayment({commit}, card) {
      commit('mutate', {type: 'cardForPayment', items: card});
    },
    setCryptoWalletForPayment({commit}, cryptoWallet) {
      commit('mutate', {type: 'cryptoWalletForPayment', items: cryptoWallet});
    },
    setSelectedCheckboxModal({commit}, variable) {
      commit('mutate', {type: 'selectedCheckboxModal', items: variable});
    },
    pushUserTransactions({commit}, transactions) {
      commit('pushUserTransactions', transactions);
    },
    updateCryptoRequisite({commit, state}, cryptoRequisite) {
      commit('updateCryptoRequisite', cryptoRequisite)
      commit('updateAvailableWithdrawalCryptoCurrenciesForAddition', state.crypto_requisites)
    },
    addCryptoRequisite({commit, state}, cryptoRequisite) {
      state.crypto_requisites[cryptoRequisite.type][cryptoRequisite.currency].push(cryptoRequisite)
      state.crypto_requisites[cryptoRequisite.type][cryptoRequisite.currency].push(cryptoRequisite)
    }
  }
}
