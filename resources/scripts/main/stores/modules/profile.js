import requests from "../../requests";

export default {
  state: {
    investment:{
      tariffs: null,
      idForMoreInfo: null,
      idForAddMemberToInvest: null,
      investsList: null,
    },
  },
  getters: {
    getTariffs: state => state.investment.tariffs,
    getIdForMoreInfo: state => state.investment.idForMoreInfo,
    getIdForAddMemberToInvest: state => state.investment.idForAddMemberToInvest,
    getInvestsList: state => state.investment.investsList
  },
  mutations: {
    setTariffs(state, data) {
      state.investment.tariffs = data
    },
    setIdForMoreInfo(state, data) {
      state.investment.idForMoreInfo = data
    },
    setIdForAddMemberToInvest(state, data) {
      state.investment.idForAddMemberToInvest = data
    },
    setInvestsList(state, data) {
      state.investment.investsList = data
    }
  },
  actions: {
    getInvestsList(ctx) {
      requests.dashboard.profile.getInvestsList()
        .then(res => {
          ctx.commit('setInvestsList', res.data.invests_list)
        })
        .catch(err => console.log(err))
    },
    updateUserData({commit}) {
      return requests.dashboard.profile.getUserData().then(({data}) => {
        commit('updateUser', data.user);
      });
    },
    updateUserCards({commit}) {
      return requests.dashboard.profile.getUserCards().then(({data}) => {
        commit('mutate', {type: 'user_cards', items: data.user_cards});
        commit('mutate', {type: 'unverified_cards', items: data.unverified_cards});
      });
    },
    updateUserBankRequisites({commit}) {
      return requests.dashboard.profile.getUserBankRequisites().then(({data}) => {
        commit('mutate', {type: 'bank_requisites', items: data.bank_requisites});
        commit('mutate', {type: 'system_bank_requisites', items: data.system_bank_requisites});
      });
    },
    updateUserCryptoRequisites({commit}) {
      return requests.dashboard.profile.getUserCryptoRequisites().then(({data}) => {
        commit('mutate', {type: 'crypto_requisites', items: data.crypto_requisites});
      });
    },
  }
}
