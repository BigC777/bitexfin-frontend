import Terminal from './modules/terminal'
import ExchangeRate from './modules/exchange-rate'
import Dashboard from './modules/dashboard'
import Config from './modules/config'
import User from './modules/user'
import Profile from './modules/profile'

import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Terminal,
    ExchangeRate,
    Dashboard,
    Config,
    User,
    Profile
  }
});
