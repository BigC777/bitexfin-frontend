/**
 * Init global libs
 */

window.jQuery = window.$ = jQuery

/**
 * Require global libs
 */
require('bootstrap/dist/js/bootstrap')
require('chosen-js/chosen.jquery')

import ReplenishmentDiscard from './admin/replenishments/ReplenishmentDiscard'
import ReplenishmentAccept from './admin/replenishments/ReplenishmentAccept'
import VerificationsDiscardAction from './admin/verifications/DiscardAction'
import VerificationsAcceptAction from './admin/verifications/AcceptAction'
import EmailVerification from './admin/users/email-verification/Index'
import UserReferrals from './admin/users/user-referrals/Index'
import ReplenishmentTableView from './admin/replenishments/TableView'
import UserLimits from './admin/users/user-limits/UserLimits'
import UserVoucherSettings from './admin/users/user-voucher-settings/UserVoucherSettings'
import UserActionsLog from './admin/users/user-actionslog/UserActionslog'
import UserEditForm from './admin/users/user-edit/UserEditForm'
import CardVerifyDiscard from './admin/cards/CardVerifyDiscard'
import CardVerifyApprove from './admin/cards/CardVerifyApprove'
import CurrencyGridView from './admin/currencies/GridView'
import LimitsView from './admin/limits/LimitsView'
import TerminalSettingsTerminalView from './admin/terminal-settings/TerminalView'
import ExternalOrdersExternalView from './admin/external-orders/ExternalView'
import ServiceManagementView from './admin/service-management/ServiceManagementView'
import ContainerBonusPercentsOnAccountView from './admin/bonus-percents-on-account/ContainerBonusPercentsOnAccountView'
import SuccessView from './common/form-elements/Success'
import ErrorView from './common/form-elements/Errors'
import LeftPanel from './admin/layouts/LeftPanel'
import Csrf from './common/utilities/csrf'
import LoginForm from './admin/login/Index'
import TransactionGridView from './admin/transactions/GridView'
import Access from './common/directives/access'
import store from './admin/stores/index'
import SystemRequisitesGridView from './admin/system-requisites/GridView'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import Datepicker from 'vue2-datepicker'
import BankRequisitesApproveAction from './admin/bank-requisites/ApproveAction'
import BankRequisitesDiscardAction from './admin/bank-requisites/DiscardAction'
import WithdrawalGridView from './admin/withdrawals/GridView'
import ExchangeGridView from './admin/exchange/GridView'
import VoucherSettingsGridView from './admin/voucher-settings/GridView'
import VouchersView from './admin/vouchers/VouchersView'
import TariffGridView from './admin/partners/tariffs/TariffGridView'
import UserRefill from './admin/users/user-refill/Index'
import EmailDeliveryIndex from './admin/email-delivery/EmailDeliveryIndex'
import BankReq from './admin/bank-requisites/BankReq'

import vuexI18n from 'vuex-i18n';
import translationsEn from './lang/en.json';
import translationsRu from './lang/ru.json';

/**
 * Set global plugins
 */
Vue.use(PerfectScrollbar);
Vue.use(Datepicker);
Vue.component('error-view', ErrorView);
Vue.component('success-view', SuccessView);
Vue.directive('access', Access);

Vue.use(vuexI18n.plugin, store);

Vue.i18n.add('EN', translationsEn);
Vue.i18n.add('RU', translationsRu);

Vue.i18n.set(localStorage.getItem('lang') || 'EN');

Csrf.setCsrf();

/**
 * Init VueJS application
 */
new Vue({
  el: '#app',
  store,
  components: {
    VerificationsDiscardAction,
    VerificationsAcceptAction,
    ReplenishmentTableView,
    CardVerifyDiscard,
    CardVerifyApprove,
    CurrencyGridView,
    UserLimits,
    UserActionsLog,
    UserEditForm,
    LeftPanel,
    ReplenishmentAccept,
    ReplenishmentDiscard,
    LoginForm,
    TransactionGridView,
    SystemRequisitesGridView,
    EmailVerification,
    BankRequisitesApproveAction,
    BankRequisitesDiscardAction,
    LimitsView,
    WithdrawalGridView,
    ExchangeGridView,
    VoucherSettingsGridView,
    VouchersView,
    TerminalSettingsTerminalView,
    ExternalOrdersExternalView,
    ServiceManagementView,
    ContainerBonusPercentsOnAccountView,
    TariffGridView,
    UserReferrals,
    UserRefill,
    UserVoucherSettings,
    EmailDeliveryIndex,
    BankReq
  },
  beforeCreate() {
    this.$store.dispatch('loadPermissions');
  }
});
