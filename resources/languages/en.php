<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main page
    |--------------------------------------------------------------------------
    */
    'calc_title' => 'Financial solutions for you',
    'calc_btn' => 'Show yield',
    // investments
    'investments_title' => 'Investment strategies',
    'your_verification_code' => 'Your code: %s',
    // as working
    'asw_title' => 'How do investment funds work?',
    'asw_get_consultation' => 'Get a consultation for free',
    'asw_column1' => 'You choose an investment package and purchase it. From now on, you become an investor, like thousands of other clients.',
    'asw_column2' => 'Fund funds are used for the acquisition of securities according to the profile of its activities. This is how the value of the investment portfolio is formed.',
    'asw_column3' => 'The investor\'s income is obtained only with an increase in the value of his assets in the portfolio. The price of a portfolio is subject to market fluctuations and can increase, decrease, and then rise again.',
    /*
    |--------------------------------------------------------------------------
    | Authenticate
    |--------------------------------------------------------------------------
    */
    'signin' => 'Sign in',
    'incorrect_login_password' => 'Incorrect login and/or password.',
    'registration' => 'Registration',
    'success_signup_message' => 'Now you can enter',
    'enter_personal_account' => 'Enter personal account',
    'login' => 'Login',
    'password' => 'Password',
    'phone_number_email' => 'Phone number/E-mail',
    'enter' => 'Enter',
    'forgot_your_password' => 'Forgot your password',
    'registration_in_personal_account' => 'Registration in personal account',
    'your_email' => 'Your E-mail',
    'repeat_password' => 'Repeat password',
    'register' => 'Register',
    'password_recovery' => 'Password recovery',
    'enter_phone_number' => 'Enter phone number',
    'confirmation_code' => 'Confirmation code',
    'recover' => 'Recover',
    'back_to_enter' => 'Back to enter',
    'incorrect_code' => 'Incorrect code',
    'by_continuing_to_use_our_site' => 'By continuing to use our site, you consent to the processing of cookies, user data (location information; OS type and version; Browser type and version; device type and screen resolution; source from where the user came to the site; what site or on what advertising; OS and Browser language; which pages the user opens and which buttons the user clicks; ip address in order to operate the site, conduct retargeting and conduct statistical studies and reviews. If you do not want your data to be processed, leave the site.',
    'your_new_password' => 'Your new password %s',
    'email_already_verified' => 'Email already verified',
    'email_verified' => 'Email verified',

    /*
    |--------------------------------------------------------------------------
    | Backend
    |--------------------------------------------------------------------------
    */
    'signin_all' => 'Incorrect login and/or password',
    'signin_all_attempt' => 'You have %s out of %d, after which the account will be blocked',
    'signin_all_ban' => 'Due to the fact that you have entered incorrect information for authorization %d times, we have blocked your account for your security. To unlock, call customer support at %s',
    'signin_attempt1' => 'attempt',
    'signin_attempt2' => 'attempts',
    'signin_attempt5' => 'attempts',
    'signin_not_allowed_ip' => 'This IP address is not on the allowed list. For more information to contact us.',
    'send_sms_failed' => 'Failed to send SMS',
    'send_mail_failed' => 'Failed to send email',
    'sms_code_verify' => 'Your code is %d',
    'sms_voucher' => 'Your hash for activation voucher: %s',
    'sms_invest' => 'Your hash for activation investment: %s',
    'passowrd_reset_subject' => 'Password recovery',
    'registration_failed' => 'Failed to register new user',
    'password_reset_sms_error' => 'Failed to send SMS',
    'success_message_for_phone' => 'SMS with code sent to your phone',
    'need_google_authentication' => 'Need google code for authentication',
    'password_reset_many_request' => 'Exceeded limit %s requests per day', // %s can be the word SMS or E-mail
    'success_message_for_email' => 'On Your Email sent an email with code',
    'user_not_found' => 'The user is not found',
    'success_send_password' => 'On your %s sent a new password to log in', // %s can be the word E-mail or phone
    'operation_failed' => 'Operation failed',
    'currency_convert_failed' => 'Failed to convert',
    'invalid_loan_tariff_name' => 'Invalid tariff name',
    'tariff_not_found' => 'Tariff not found',
    'tariff_incorrect_min_sum' => 'The entered amount is less than required by the tariff',
    'tariff_incorrect_max_sum' => 'The entered amount is more than allowed by the tariff',
    'tariff_not_enough_money' => 'Insufficient funds on %s account: %d', // %s can be the word "current" or "investment", %d do not translate
    'get_currency_rate_failed' => 'Failed to get rate',
    'tr_comment_ag' => 'Fix Fee payment - %s',
    'tr_comment_in' => 'Pay for Money management',
    'tr_comment_pin' => 'Incoming payment %d lvl %s',
    'tr_comment_pin_private' => 'Incoming payment %d lvl. - PS %s',
    'investment_not_found' => 'Investment not found',
    'deposit_permission_failed' => 'No access to deposit',
    'deposit_not_found' => 'Deposit not found',
    'deposit_alert_message_on_return' => 'Attention, you are closing your deposit ahead of schedule! This will start recalculating all charges. Withdrawal will be realized in 10 days.',
    'investment_cancel_income_is_null' => 'It is impossible to close the investment with no income',
    'investment_already_closed' => 'Cannot close what is closed=)',
    'tr_comment_ret_mgfee' => 'Return Management Fee at recalculation',
    'tr_comment_ret_ind' => 'Write-off of all profit from the account at recalculation',
    'tr_comment_new_ind' => 'New profit at recalculation',
    'tr_comment_new_mgfee' => 'New management fee at recalculation',
    'investment_cancel_unknown_error' => 'Unexpected error occurred',
    'pledge_not_found' => 'No Deposit found',
    'bank_requisite_not_found' => 'Requisite not found',
    'tr_comment_ag_ps' => 'Payment of Commission Fix Fee P/S',
    'tr_comment_bcc' => 'Payment for services P/S',
    'tr_comment_pin_ps' => 'Incoming payment %d lvl P/S %s',
    'tr_comment_pin_private_ps' => 'Incoming payment %d lvl P/S - PS%s',
    'tr_comment_ret_bcc' => 'Refund of service fees P/S',
    'tr_comment_rtc' => 'Refund at closing of P/S',
    'tr_comment_inps' => 'Incoming transaction Income',
    'tr_comment_sfps' => 'Outgoing transaction Success Fee',
    'tr_comment_lps' => 'Outgoing transaction Loss',
    'tr_comment_dps' => 'Outgoing transaction Dept',
    'send_notification_failed' => 'Failed to send notification',
    'notification_title_ps' => 'Pledge storage',
    'notification_message_store_ps' => 'User ID %d has sent a request for pledge creation',
    'notification_message_close_ps' => 'User ID %d sent a request to close pledge #%d',
    'notification_message_accept_ps' => 'Pledge #%d approved',
    'notification_message_discard_ps' => 'Pledge #%d declined',
    'notification_message_closing_accept' => 'Request for pledge closure #%d approved',
    'notification_message_closing_discard' => 'Request for pledge closure #%d declined',
    'notification_not_found' => 'Notification not found',
    'pledge_not_enough_money' => 'Not enough money on balance',
    'currency_not_found' => 'Currency not found',
    'pledge_unknown_currency' => 'Invalid currency',
    'replenishment_not_found' => 'Application for replenishment is not found',
    'tr_comment_cw' => 'Incoming transaction Account replenishment',

    'notification_message_replenish_currennt_account_by_admin' => 'Your Current account is credited with: %d %s',
    'notification_message_replenish_investment_account_by_admin' => 'Your Investment account is credited with: %d %s',
    'notification_message_replenish_bitcoin_accountby_admin' => 'Your Bitcoin account is credited with: %d %s',
    'notification_message_replenish_ethereum_account_by_admin' => 'Your Ethereum account is credited with: %d %s',
    'notification_message_replenish_partner_account_by_admin' => 'Your Partner account is credited with: %d %s',

    'currency_rate_not_found' => 'Currency rate not found',
    'required_hash' => 'Specify hash',
    'required_full_name_admin' => 'Specify recipient',
    'required_full_client_data' => "Fill sender's details",
    'notification_replenish_title' => 'Replenishment',
    'notification_replenish_message' => 'User ID %d has sent a request #%d for replenishment',
    'user_age_invalid' => 'You cannot register users under the age of 18',
    'agent_not_found' => 'Partner not found',
    'upload_images_failed' => 'Failed to load image',
    'partner_signup_failed' => 'Failed to register user',
    'notification_verification_title' => 'Verification',
    'notification_verification_message' => 'User ID %d has sent you request for verification',
    'role_not_found' => 'Role is not found',
    'role_already_exist' => 'Role already assigned',
    'account_not_found' => 'Account not found',
    'incorrect_sum' => 'Invalid number',
    'tr_comment_writeoff_default' => 'Administrator funds cancellation',
    'user_incorrect_status' => 'Incorrect user status',
    'office_not_found' => 'Office not found',
    'notification_verification_success_message' => 'You have been verified',
    'notification_verification_failed_message' => 'You have not passed the verification of identity. Reason: %s',
    'withdrawal_not_found' => 'Application for withdrawal of funds not found',
    'withdrawal_set_paid' => 'Request #%d for withdrawal is marked as paid',
    'notification_withdrawal_title' => 'Withdrawal of funds',
    'notification_withdrawal_message_accept' => 'Request #%d for withdrawal is approved',
    'notification_withdrawal_message_send' => 'User #%d submitted request #%d for withdrawal',
    'user_invalid_state' => 'Invalid state',
    'user_invalid_data' => 'Fill your personal data',
    'user_invalid_address' => 'Fill your registration address',
    'user_invalid_passport_data' => 'Fill your passport data',
    'user_invalid_document_data' => 'Download all required documents',
    'send_code_failed' => 'Failed to send code',
    'code_does_not_exist' => 'Code not found',
    'code_error_limit' => 'You have exceeded the number of errors when entering code.',
    'user_wallet_already_exist' => 'Wallet already occupied',
    'user_already_exist' => 'User with this phone number or email is already registered',
    'email_already_exist' => 'This email already exists',
    'phone_number_already_exist' => 'This phone number already exists',
    'code_request_limit' => 'Limit of confirmation code requests exceeded',
    'user_not_verified' => 'You are not verified',
    'withdrawal_enough_money' => 'Not enough money',
    'tr_comment_pw' => 'Cancellation while withdrawing funds',
    'document_upload_failed' => 'At least %d documents are required for upload.',
    'document_upload_required' => '%d documents are required for upload.',
    'document_limit' => 'Document download limit exceeded',
    'document_incorrect' => 'Invalid document size and / or type #%d',
    'document_required' => 'Document #%d is required',
    'phone_change_confirm' => 'Confirmation to change phone number',
    'need_confirm' => 'Need confirmation',

    // Validator
    'validator_length' => 'The value has to be not less {{minValue}}',
    'validator_notEmpty' => 'The field cannot be empty',
    'validator_email' => 'The entered data are not E-mail',
    'validator_equals' => 'Fields do not match',
    'validator_phone' => 'The entered data are not phone',
    'validator_oneOf' => 'Incorrect data',
    'validator_positive' => 'Not the positive number is entered',
    'validator_intVal' => 'The value is not integer',
    'validator_floatVal' => 'The value is not number with a floating point',
    'validator_intType' => 'Not the positive number is entered',
    'validator_numeric' => 'Incorrect number',
    'validator_negative' => 'The number should not be negative',
    'validator_date' => 'Incorrect date',
    'validator_floatType' => 'Incorrect number',
    'validator_min' => 'The value has to be not less {{minValue}}',
    'validator_image' => 'The loaded type is not the image',
    'validator_size' => 'Incorrect size',
    'validator_surname' => 'Only Russian and English letters, "-" and space are allowed.',
    'validator_street' => 'Only Russian and English letters, numbers, "-", "/" and space are allowed.',
    'validator_house_number' => 'Only Russian and English letters, numbers and "/" are allowed.',
    'validator_department_code' => 'Only numbers and "-" are allowed.',
    'validator_recipient_address' => 'Only Russian and English letters, numbers, space, ",", "-", "/" are allowed.',
    'validator_only_ru_en_letters' => 'Only Russian and English letters are allowed.',
    'validator_only_ru_en_letters_and_space' => 'Only Russian and English letters and space are allowed.',
    'validator_in_array' => 'Incorrect value',
    'validator_pasport_series' => 'User with this data already exist',
    'validator_pasport_number' => 'User with this data already exist',

    // message
    'change_private_data' => 'Personal details have been changed',
    'change_address' => 'The registration address has been changed',
    'change_passport' => 'Passport details have been changed',
    'upload_documents' => 'Documents successfully uploaded',
    'change_phone_email' => 'Phone and Email have been changed',
    'change_verification' => 'Pending verification application',
    'change_password_success' => 'Password has been changed',
    'change_bitcoin' => 'Bitcoin wallet has been changed',
    'change_ethereum' => 'Ethereum wallet has been changed',
    'change_password' => 'Password has been changed',

    // Send email Reset password
    'reset_password_subject' => 'Bitexfin password change',
    'reset_password_message' => 'You have changed the password for authorization. If you did not change your password, contact technical support, send an email to: support@bitexfin.com',
    'incorrect_password_error' => 'Password must be at least 8 characters with one uppercase letter and one number (any special characters is allowed except "-" , "+" , "=")',

    // Frontend

    // on request
    'application_on_consideration' => 'Application on consideration',
    'request_sent' => 'Application is sent, please wait...',
    'operation_success' => 'Operation completed successfully',

    'deposit_contract_number' => 'Deposit agreement number',
    'change_password_title' => 'Change password from personal account',
    'bank_requisite_modal_text_on_delete' => 'Are you sure?',

    // Money accounts
    'current_account' => 'Current account',
    'investment_account' => 'Investment account',
    'bitcoin_account' => 'Bitcoin account',
    'ethereum_account' => 'Ethereum account',
    'affiliate_account' => 'Affiliate account',
    'salary_account' => 'Salary account',

    // Dashboard Menu
    'money_management' => 'Money management',
    'pledge_storage' => 'Pledge storage',
    'question-answer' => 'Question-answer',
    'support' => 'Support',
    'loan' => 'Loan transactions',

    'loan_request_title' => 'Loan application confirmation',
    'loan_request_message' => 'Customer %fullname% executed a loan request on the security of your deposit',
    'loan_request_success_confirm' => 'Loan application successfully confirmed',
    'loan_request_reject' => 'Loan application rejected',
    'close' => 'Close',
    'cancel' => 'Cancel',

    // Partner link
    'referral_program' => 'Referral program',
    'referral_program_text' => 'Copy your referral link and give it to your partner',

    // Leadership Bonus
    'leadership_bonus_rank' => 'Rank',

    // Affiliate Menu
    'personal_clients' => 'Personal clients',

    // Loan Menu
    'menu_loan_active_request' => 'Active requests',
    'menu_loan_active_lender' => 'Active Lender',
    'menu_loan_active_paymaster' => 'Active Paymaster',
    'menu_loan_active_cashier' => 'Active Cashier',
    'menu_loan_issued' => 'Issued',
    'menu_loan_refused' => 'Refused',

    'welcome' => 'Welcome!',
    'greeting' => 'Welcome, in order to receive registration, you must choose the currency of the Conventional currency:',

    // Currency
    'currency' => 'Currency',
    'ruble' => 'Ruble',
    'hryvnia' => 'Hryvnia',
    'tenge' => 'Tenge',
    'dollar' => 'Dollar',

    // Create loan request
    'choose_deposit' => 'Select a deposit',
    'choose_office' => 'Select office',
    'loan_currency' => 'Loan currency',
    'min_loan_amount' => 'Minimum amount',
    'max_loan_amount' => 'Maximum amount',
    'get_loan_tariff_failed' => 'Failed to get product info',
    'loan_max_amount_invalid' => 'The amount entered exceeds the amount of the deposit',
    'office' => 'Office',
    'loan_sum' => 'Loan amount',
    'status' => 'Status',
    'pay_off_debt' => 'Pay off debt',
    'loan_request' => 'Loan',

    // Admin Loan
    'loan_notification_title' => 'Заявки на займ',
    'accept_loan_notification_message' => 'Заявка на займ #%d одобрена',
    'discard_loan_notification_message' => 'Заявка на займ #%d была отклонена по причине: %s',

    'save' => 'Save',
    'portfolios' => 'Portfolios',
    'deposit' => 'Deposit',
    'deposit_amount' => 'Deposit amount',
    'from' => 'From',
    'to' => 'To',
    'profitableness' => 'Profitableness',
    'in_month' => 'per month',
    'in_year' => 'per year',
    'term_of_work' => 'Term of work',
    'mth' => 'month',
    'year' => 'year',
    'invest' => 'Invest',
    'privacy_policy' => 'Privacy policy',
    'user_agreement' => 'Terms of use',

    // Header menu
    'my_profile' => 'My profile',
    'personal_account' => 'Personal account',
    'affiliate_profile' => 'Affiliate profile',
    'logout' => 'Logout',

    'specify_the_amount_to_invest' => 'Specify the amount to invest from the desired account',
    'enter_amount' => 'Enter amount',
    'pay' => 'Pay',
    'i_have_read' => 'I have read',
    'agreement_and_accept_its_terms' => 'agreement and accept its terms',
    'return_deposit' => 'Return deposit',
    'request_for_funds_withdrawal' => 'Request for funds withdrawal',
    'request_for_funds_withdrawal_rejected' => 'Withdrawal application #%s rejected. Reason: %s',
    'request_for_funds_deposit_rejected' => 'Deposit application #%s rejected. Reason: %s',
    'replenish' => 'Replenish',
    'funds_withdrawal' => 'Funds withdrawal',
    'funds_deposit' => 'Funds deposit',
    'to_withdraw_funds_you_need_to' => 'To withdraw funds, you need to pass verification, fill in',
    'payment_details' => 'Payment details ',
    'and_have_a_positive_balance' => 'and have a positive balance',
    'transaction_history' => 'Transaction history',
    'total' => 'total',
    'output_by' => 'Output by',
    'operation_type' => 'Operation type',
    'all' => 'All',
    'outbox' => 'Outbox',
    'inbox' => 'Inbox',
    'payment_date_from' => 'Payment date from',
    'payment_date_to' => 'Payment date to',
    'refresh' => 'Refresh',
    'transaction' => 'Transaction',
    'amount' => 'Amount',
    'order' => 'Order',
    'date' => 'Date',
    'comment' => 'Comment',
    'deposit_request' => 'Deposit request',
    'notify_external_deposit_email' => '%s sent you a money request for %s %s <br> <a href="%s">Pay</a>',
    'notify_external_deposit_sms' => '%s sent you a money request for %s %s. Pay %s',
    'enter_transaction_hash' => 'Enter transaction hash',
    'minimum_balance_replenishment_amount' => 'Minimum balance replenishment amount',
    'minimum_balance_withdrawal' => 'Minimum withdrawal amount',
    'rate' => 'Rate',
    'payment_method' => 'Payment method',
    'pay_bill' => 'Pay the bill',
    'not_selected'=>'Not chosen',
    'receipt_scan'=>'Receipt scan',
    'uploaded_file_types'=>"Types of uploaded file: 'pdf', 'png', 'jpeg', 'jpg', 'gif'",
    'file_max_size'=>"Maximum size 4 Mb.",
    'transaction_hash'=>"Transaction HASH",
    'invoice'=>'Invoice',
    // Wallets
    'bitcoin_wallet' => 'Bitcoin wallet',
    'ethereum_wallet' => 'Ethereum wallet',

    'perform_amount_transfer' => 'Perform amount transfer specified in the line, then click (Pay)',
    'confirm' => 'Confirm',
    'available' => 'Available',
    'your_bitcoin_wallet' => 'Your bitcoin wallet',
    'withdrawal_of_funds_from_this_account' => 'Withdrawal of funds from this account is made within 72 hours from the sent application date.',

    'active_positions' => 'Active positions',
    'tariff' => 'Tariff',
    'days' => 'days',
    'income' => 'Income',
    'active' => 'Active',
    'closed' => 'Closed',
    'processing' => 'Processing',
    'consideration' => 'Pending',
    'canceled' => 'Canceled',
    'rejected' => 'Rejected',
    'manage' => 'Manage',
    'contract_number' => 'Contract number',
    'investment_date' => 'Investment date',
    'percent_per_month' => 'Percent per month',
    'contract' => 'Contract',
    'download_contract' => 'Download contract',
    'closing_date' => 'Closing date',

    // Pledges
    'create_request' => 'Create request',
    'deposit_id' => 'Deposit ID',
    'pledge_amount' => 'Deposit amount',
    'leverage' => 'Leverage',
    'gross_income' => 'Gross income',
    'processing_close' => 'Awaiting closure',
    'operation' => 'Operation',
    'current_rate' => 'Current rate',
    'deposit_rate' => 'Deposit rate',
    'current_rate_value' => 'Current rate',
    'profit_loss' => 'Profit/Loss',
    'close_position' => 'Close position',
    'trading_position' => 'Trading position',
    'total_to_return' => 'Итого к возврату',
    'fulfill' => 'Fulfill',
    'fulfill_pledge' => 'Fulfill pledge',
    'from_current_crypto-account' => 'From current crypto-account',
    'price' => 'Price',
    'next' => 'Next',
    'enter_the_hash' => 'Enter the hash of the completed transaction',

    // Support
    'happy_to_help' => 'Happy to help! Just contact us.',
    'phone_number' => 'Phone number',
    'support_service_hours' => 'Support service hours: Mon-Fri from 09:00 to 18:00 UTC+3',
    'support_service_hours_personal' => 'Support service hours',
    'provide_the_correct_information' => 'Please provide the correct information for identification and a more detailed understanding of the situation. Be sure to specify your ID %user_id% this will allow the employee to contact you soon and provide the most complete response to your request.',
    'ask_question'=>'Ask a question',
    'additional_contacts'=>'Additional contacts',

    // Profile
    'user_account' => 'User account',
    'personal_data' => 'Personal data',
    'residence_address' => 'Residence address',
    'passport_details' => 'Passport details',
    'add_document' => 'Add document',
    'phone_and_email' => 'Phone number and E-mail',
    'verification' => 'Verification',
    'security' => 'Security',
    'security_hint'=>'Come up with a long password of letters and numbers that will be easy for you to remember, and difficult for others to guess.',
    'personal_account_password' => 'Personal account password',
    'documents'=>'Documents',
    'edit'=>'Edit',
    'user_account_hint'=>'You can change your personal data, address of registration, telephone, e-mail and upload documents.',


    // Profile -> Personal Data
    'surname' => 'Surname',
    'name' => 'Name',
    'patronymic' => 'Patronymic',
    'date_of_birth' => 'Date of birth',
    'sex' => 'Sex',
    'male' => 'Male',
    'female' => 'Female',
    'citizenship' => 'Citizenship',

    // Profile -> Residence address
    'country' => 'Country',
    'city' => 'City',
    'street' => 'Street',
    'building' => 'Building',
    'apartment' => 'Apartment',
    'we_need_your_address_to_verify' => 'We need your address to verify your account. Information about the address must be reliable and supported by documents',

    // Profile -> Passport details
    'series' => 'Series',
    'number' => 'Number',
    'date_of_issue' => 'Date of issue',
    'department_code' => 'Department code',
    'issued' => 'Issued',

    // Profile -> Add Documents
    'uploaded_documents' => 'Uploaded documents',
    'id' => 'ID',
    'download_one_of_the_following_documents' => 'Download one of the following documents',
    'national_passport' => 'National passport (spread)',
    'foreign_passport' => 'Foreign passport (spread)',
    'driving_license' => 'Driving license',
    'remove' => 'Remove',
    'document_type' => 'Document type',
    'your_photo_with_document' => 'Your photo with document',
    'photo_with_you' => 'Photo with You',
    'photo_with_you_big' => 'Photo with You holding an identification document and a sheet of paper with the current date and the phrase custodianfund.io written manually.',
    'residential_address' => 'Residential address',
    'utility_bill' => 'Utility bill',
    'document_confirming_your_residence' => 'Document confirming your residence and issued by local authorities',
    'bank_statements_or_other_documents' => 'Bank statements or other documents indicating the actual address of residence.',
    'upload' => 'Upload',
    'we_appreciate_your_time' => 'We appreciate your time! Therefore, please send documents that meet the following requirements',
    'copies_match_to_the_original' => 'Copies match to the original',
    'all_documents_are_valid' => 'All documents are valid at the sending moment',
    'high_quality_images' => 'High quality (color images with a resolution of 300 dpi or more)',
    'file_size_not_less_than' => 'File size not more than 2 MB',
    'document_confirming_the_place_of_residence' => 'Document confirming the place of residence must be no older than 6 months and it must contain your name',
    'we_do_not_accept_scans' => 'We do not accept scans, screenshots and PDFs – please upload only photos of required documents',
    'photos_should_not_be_subjected' => 'Photos should not be subjected to any kind of processing – please do not compress, crop, rotate images or edit them in any other way',

    // Profile -> Change Password
    'old_password' => 'Old password',
    'new_password' => 'New password',
    'repeat_new_password' => 'Repeat new password',
    'error_new_password' => 'The new password must be different from the old one',
    'sms_code' => 'SMS code',
    'request_code' => 'Request Code',
    'in_order_not_to_become_a_victim_of_fraud' => 'In order not to become a victim of fraud, you must set a complex password to your personal account.',
    'our_minimum_requirements_for_a_strong_password' => 'Our minimum requirements for a strong password',
    'password_must_be_at_least' => 'password must be at least 8 characters long',
    'password_must_contain_at_least' => 'password must contain at least one uppercase letter (A to Z) and one lowercase letter (a to z) of Latin alphabet',
    'password_must_not_contain_a_trivial_sequence' => 'password must not contain a trivial sequence of characters, for example: Qwerty123 or 12345678',
    'password_must_not_match' => 'password must not match any of the previously used on the site',
    'if_you_forget_your_old_password' => 'If you forget your old password, please use the password recovery form or contact support',

    // Profile -> Change Phone number AND E-mail
    'current_email' => 'Current e-mail',
    'current_phone_number' => 'Current phone number',
    'confirmed' => 'Confirmed',
    'enter_code' => 'Enter code',



    //Profile -> Bank requisites
    'bank_requisites'=>'Bank requisites',
    'payment_requisites'=>'Payment requisites',
    'full_name_placeholder'=>'Full name(name of company)',
    'inn'=>'INN',
    'ogrn'=>'OGRN',
    'kpp'=>'KPP',
    'bik'=>'BIK',
    'payment_account'=>'Payment account(or card number)',
    'commercial_account'=>'Commercial account',
    'bank'=>'Bank',
    'exceeded_type_limit'=>'Exceeded limit for adding bank details of this type',




    // Profile -> Verification
    'verification_request' => 'Verification request',

    // Profile -> Api
    'keys_limit' => 'Exceeded limit for adding keys',

    //
    'client_registration' => 'Client registration',
    'office_' => 'office',
    'agent_id' => 'Agent ID',
    'send' => 'Send',
    'full_name' => 'Full name',
    'registration_date' => 'Registration date',


    //FEES

    'balance' => 'Balance',

    //Investments
    'investments'=>'Investments',
    'events'=>'Events',
    'create_investment'=>'Create investment',
    'add'=>'Add',
    'paid'=>'Paid',

    //User cards
    'only_3d_secure'=>'Your card is not supports 3DS',
    'service_unavailable'=>'Service is temporarily unavailable',
    'card_successfully_added'=>'Card successfully added',
    'card_already_exists'=>'The card is already attached',
    'card_already_exists_message'=>'The card with number %s is already attached',
    'card_not_found' => 'Card not found',



    // Email messages
    'email_subject_change_password'=>'Bitexfin password change',
    'email_message_change_password'=>'You have changed the password for authorization. If you did not change your password, contact technical support, send an email to: support@bitexfin.com',

    'email_subject_exchange_operation'=>'Bitexfin exchange operations',
    'email_message_exchange_operation'=>'You exchanged %s %s for %s %s <br> The exchange fee is: %s %s',

    'email_subject_verification_approved'=>'Bitexfin verification',
    'email_message_verification_approved'=>'You have successfully passed the verification. Now you have access to the exchange transaction.',

    'email_subject_verification_declined'=>'Bitexfin verification',
    'email_message_verification_declined'=>'You did not pass verification. Reason: %s',

    'email_subject_login'=>'Bitexfin authorization',
    'email_message_login'=>'You are logged in to your personal account %s <br> If you did not log in, be sure to change your password.',

    'email_subject_email_confirmation'=>'Bitexfin email confirmation',
    'email_message_email_confirmation'=>'To complete the registration, confirm your email address. To do this, follow the link: <a href="%s">Confirm</a>',

    'email_subject_voucher_send'=>'Bitexfin Voucher Activation',
    'email_message_voucher_send'=>'%s %s!<br>A voucher of %s %s  has been sent to you. <br>Sender: %s<br>To activate, click on the link: %s',

    'email_subject_voucher_send_eurobaras'=>'Funds from Eurobaras',
    'email_message_voucher_send_eurobaras'=>'Hello, your account has received funds from Eurobaras %s %s',

    'email_subject_investment_send'=>'Bitexfin Investment Activation',
    'email_message_investment_send'=>'%s %s <br>A investment of %s %s  has been sent to you. <br>Sender: %s<br>To activate, click on the link: %s',

    'bitexfin_card_verification' => 'Bitexfin card verification',
    'your_card_failed_verification' => 'Your card %s failed verification. Reason: %s',
    'your_bank_requisites_passed_verification' => 'Your bank requisites passed the verification %s',
    'bitexfin_bank_requisites_verification' => 'Bitexfin Bank requisites verification',
    'bitexfin_payment_details' => 'Bitexfin payment details',
    'payment_completed' => 'Payment completed! <br> Recepient: %s %s <br> ID: %s <br> Date: %s <br> Total: %s %s <br>',
    'order_was_created_successfully' => 'Order was created successfully! <br>&nbsp;&nbsp;&nbsp;id: %s <br>&nbsp;&nbsp;&nbsp;pair: %s <br>&nbsp;&nbsp;&nbsp;side: %s <br>&nbsp;&nbsp;&nbsp;type: %s <br>&nbsp;&nbsp;&nbsp;timestamp: %s',

    // Fill profile message
    'fill_profile' => "Please fill in your profile",
    'verify_profile' => 'Please apply verification',
    // Confirm user email
    'success_confirm_user_email' => 'Thank you for registering Bitexfin.<br>Now you can log in to your account',


    'bank_requisites_declined_title'=>'Bank requisites declined',
    'bank_requisites_declined_text'=>'Bank requisites %s declined. Reason: %s',

    // Cards verification
    'verification_rejected_successful' => 'Verification rejected successfully!',
    'verification_rejected_error' => 'Verification rejected error',
    'verification_successful' => 'Verification successful!',
    'verification_error' => 'Verification failed',

    'add_card_success_subject' => 'Successful addition',
    'add_card_error_subject' => 'Error adding a bank card',
    'add_card_success' => 'Card successfully added',
    'add_card_error' => ' An error occurred while adding your bank card. Check available funds',

    // user limit errors
    'user_limit_errors_empty' => 'The operation failed. Contact customer support by phone %s'
];
